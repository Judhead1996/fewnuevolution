import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeforeloginPage } from './beforelogin.page';

describe('BeforeloginPage', () => {
  let component: BeforeloginPage;
  let fixture: ComponentFixture<BeforeloginPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeforeloginPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeforeloginPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
