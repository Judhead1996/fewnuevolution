import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';


@Component({
  selector: 'app-beforelogin',
  templateUrl: './beforelogin.page.html',
  styleUrls: ['./beforelogin.page.scss'],
})
export class BeforeloginPage implements OnInit {

  user: boolean = false;
  constructor(private router:Router, 
    public loadingCtrl: LoadingController) { }

  ngOnInit() {
    /*if(localStorage.getItem("token")) {
      this.user = true;
      this.router.navigateByUrl('/home');
    }*/
  }

  async waiting(){
    var loader = await this.loadingCtrl.create({
      message: "Please Wait...",
      duration: 20000
    });
    await loader.present();

    loader.dismiss();
  }

}
