import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  
  { path: '', redirectTo: 'beforelogin', pathMatch: 'full' },
  { path: 'beforelogin', loadChildren: './beforelogin/beforelogin.module#BeforeloginPageModule' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'inscription', loadChildren: './inscription/inscription.module#InscriptionPageModule' },
  { path: 'calculatrice', loadChildren: './calculatrice/calculatrice.module#CalculatricePageModule' },
  { path: 'authentication', loadChildren: './auth/authentication/authentication.module#AuthenticationPageModule' },
  { path: 'login', loadChildren: './auth/login/login.module#LoginPageModule' },
  { path: 'menu', loadChildren: './boutique/menu/menu.module#MenuPageModule' },

  { path: 'vente-detail/:id/:Designation/:Prix/:Client/:Date', loadChildren: './boutique/details/vente-detail/vente-detail.module#VenteDetailPageModule' },
  { path: 'depense-detail/:id/:Designation/:Prix/:Date', loadChildren: './boutique/details/depense-detail/depense-detail.module#DepenseDetailPageModule' },
  { path: 'pret-detail/:id/:Client/:Telephone/:Designation/:Montant/:Date', loadChildren: './boutique/details/pret-detail/pret-detail.module#PretDetailPageModule' },
  
  { path: 'pret-ajout', loadChildren: './boutique/details/pret-ajout/pret-ajout.module#PretAjoutPageModule' },

  { path: 'menu', loadChildren: './stock/menu/menu.module#MenuPageModule' },

  { path: 'entree-detail/:id/:Produit/:Prix_Unitaire/:Quantite', loadChildren: './stock/details/entree-detail/entree-detail.module#EntreeDetailPageModule' },
  { path: 'sortie-detail/:id/:Produit/:Prix_Unitaire/:Quantite/:Client', loadChildren: './stock/details/sortie-detail/sortie-detail.module#SortieDetailPageModule' },

  { path: 'select-entree', loadChildren: './stock/ajouter/select-entree/select-entree.module#SelectEntreePageModule' },

  { path: 'sortie-ajout/:id/:Produit/:Prix_Unitaire/:Quantite/:Client', loadChildren: './stock/ajouter/sortie-ajout/sortie-ajout.module#SortieAjoutPageModule' },
  { path: 'menustock', loadChildren: './stock/menustock/menustock.module#MenustockPageModule' },






  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
