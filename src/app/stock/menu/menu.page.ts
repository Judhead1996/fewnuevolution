import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  constructor(private router:Router, private alertController: AlertController) { }

  prenom = localStorage.getItem('nom');

  ngOnInit() {
  }

  public acc = [
    {title1:"Accueil", url:"/home", icon1:'ios-home'},
  ]

  public menus1 = [

    
    /* {title1:"Gestion stock", url:"menustock/gestionstckok", icon1:'ios-cart'},  */  
    {title1:"Historique entrées", url:"menu/hist-entree/journalier", icon1:'ios-arrow-round-forward'},    
    {title1:"Historique sorties", url:"menu/hist-sortie/journalier1", icon1:'ios-arrow-round-back'},    
   ]

   onMenu1Item(m){
    this.router.navigateByUrl(m.url);

  }

  public menu2 = [

    {title1:"Nous Contactez", url:"menu/nouscontacter", icono:'ios-contacts'},
   ]

   
  public menu1 = [

    {title:"A Propos", url:"menu/aprospos", icon:'ios-help-circle-outline'},  
   ]
   

  async onMenuo1Item(m){
    const alert = await this.alertController.create({
      header: 'Fewnu',
      message:"Fewnu est une application mobile de Gestion de boutique ou de business qui fonctionne sans connexion internet. Elle vous permet d'enregistrer vos ventes, vos dépenses et vos prêts quotidiens, de faire le résumé journalier, hebdomadaire et mensuel de vos ventes et dépenses ainsi que l'inventaire   L'application Fewnu vous permet de gérer aussi votre stock c'est à dire les entrées et les sorties de votre boutique.Avec Fewnu tenez votre boutique en main!!",
      buttons : [
        {
          text: 'Ok',
        }
      ],
    });
    alert.present();
  }

  async onMenuoItem(m){
    const alert = await this.alertController.create({
      header: 'CONCTEZ NOUS',
      
      message: "'TEL: 33867-75-08', 'PORT:78293-36-56', 'E-MAIL: contac.fewnu@gmail.com' ,'ok'",
      subHeader:' ',             
    });
    alert.present();
  }

  public logo = {
    logo : "assets/images/money.png",

  }

}
