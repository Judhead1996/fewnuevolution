import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectEntreePage } from './select-entree.page';

describe('SelectEntreePage', () => {
  let component: SelectEntreePage;
  let fixture: ComponentFixture<SelectEntreePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectEntreePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectEntreePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
