import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PublicService } from 'src/app/services/data-api/public.service';


@Component({
  selector: 'app-select-entree',
  templateUrl: './select-entree.page.html',
  styleUrls: ['./select-entree.page.scss'],
})
export class SelectEntreePage implements OnInit {

  result : any ;

  constructor(private route:Router, public  publicService: PublicService) { }

  ngOnInit() {
    this.onLoadEntree();
  }

  onLoadEntree() {
    this.publicService.getEntree()
    .then(data=>{
    
      this.result = JSON.parse(data.data)


    });
  }

  doRefresh(event) {
    this.publicService.getEntree()
    .then(data=>{
    
      this.result = JSON.parse(data.data)

    });
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }
}
