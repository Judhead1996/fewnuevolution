import { Component, OnInit } from '@angular/core';
import { PublicService } from 'src/app/services/data-api/public.service';
import { AlertController, NavController, LoadingController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-sortie-ajout',
  templateUrl: './sortie-ajout.page.html',
  styleUrls: ['./sortie-ajout.page.scss'],
})
export class SortieAjoutPage implements OnInit {

  compareWithFn = (o1, o2) => {
    return o1 && o2 ? o1.id === o2.id : o1 === o2;
  };

  result: any;
  reesult = [];
  id = "";
  Produit = "";
  Prix_Unitaire = "";
  Quantite = "";
  Client = "";
  iiPrix_Unitaire: any;
  iiQuantite: any;

  Produiiit = "";

  constructor(private route: Router, public publicService: PublicService, private router: Router, 
    private activatedRoute: ActivatedRoute, private alertController: AlertController,
    public navCtrl: NavController,
    public loadingCtrl: LoadingController) { }

  ngOnInit() {
    this.onLoadEntree();
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.Produit = this.activatedRoute.snapshot.paramMap.get('Produit');
    this.Prix_Unitaire = this.activatedRoute.snapshot.paramMap.get('Prix_Unitaire');
    this.Quantite = this.activatedRoute.snapshot.paramMap.get('Quantite');
    this.Client = this.activatedRoute.snapshot.paramMap.get('Client');
    console.log("prix", this.Prix_Unitaire)
    localStorage.setItem('idSorti', this.id)
    localStorage.setItem('produiiit', this.Produit)
    // localStorage.setItem('idEntre', this.id)
  }

  onLoadEntree() {
    this.publicService.getEntree()
      .then(data => {

        this.result = JSON.parse(data.data);

        this.listing();

      });
  }

  doRefresh(event) {
    this.publicService.getEntree()
      .then(data => {

        this.result = JSON.parse(data.data)

        this.listing();
      });
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }

  listing() {


    this.result.data.forEach(produit => {

      console.log(this.reesult.push(produit))


    });


  }

  async onAjout(sorti) {
    var loader = await this.loadingCtrl.create({
      message: "Chargement...",
      duration: 2000
    });
    await loader.present();

    var id = localStorage.getItem('userId');
    var p = localStorage.getItem('produiiit');
    if (sorti.Quantite < this.Quantite) {
      this.publicService.ajoutsortie({
        Produit: p, // sorti.Produit
        Prix_Unitaire: sorti.Prix_Unitaire,
        Quantite: sorti.Quantite,
        Client: sorti.Client,
        id_users: id,
      })
        .then(data => {
          
          if (data) {
            loader.dismiss();
            let iPrix_Unitaire = parseInt(this.Prix_Unitaire) - sorti.Prix_Unitaire;
            let iQuantite = parseInt(this.Quantite) - sorti.Quantite;

            this.iiPrix_Unitaire = iPrix_Unitaire;
            this.iiQuantite = iQuantite;

            // console.log(" = ", iPrix_Unitaire)
            // console.log(" = ", iQuantite)

            this.publicService.ajoutHistoSortie({
              Produit: p,
              Prix_Unitaire: sorti.Prix_Unitaire,
              Quantite: sorti.Quantite,
              Client: sorti.Client,
              id_users: id,
            }) 

            if(this.iiQuantite == 0){
              this.deleteEntree(id);
            }else{
              this.onUpdateEntree();
            }
           

            this.router.navigateByUrl('/menustock/accueil/sortie');
            console.log('ajout réussi 1')
          } else {
            this.router.navigateByUrl('ajoutvente');
            console.log('échec')
          }
        });
    } else if (sorti.Quantite == this.Quantite) {
      var loader = await this.loadingCtrl.create({
        message: "Chargement...",
        duration: 2000
      });
      await loader.present();

      this.publicService.ajoutsortie({
        Produit: p, //sorti.Produit
        Prix_Unitaire: sorti.Prix_Unitaire,
        Quantite: sorti.Quantite,
        Client: sorti.Client,
        id_users: id,
      })
        .then(data => {
        
          if (data) {
            loader.dismiss();
            this.router.navigateByUrl('/menustock/accueil/sortie');
            console.log('ajout réussi 2')
            /////// Supprimer ce produit de l'entree
            this.deleteEntree(id);

            this.publicService.ajoutHistoSortie({
              Produit: p,
              Prix_Unitaire: sorti.Prix_Unitaire,
              Quantite: sorti.Quantite,
              Client: sorti.Client,
              id_users: id,
            }) 
          } else {
            this.router.navigateByUrl('');
            console.log('échec')
          }
        });
    }
    else {
      const alert = await this.alertController.create({
        header: 'Erreur',
        message: "La quantité demandée est superieure à celle existante",
        buttons: ['ok'],
      });
      await alert.present();
    }

  }

  deleteEntree(id) {
    id = localStorage.getItem('idSorti');
    this.publicService.deleteEntree(id).then(() => {
    })
  }

  onUpdateEntree(){ 
    var id = localStorage.getItem('userId'); 
    var p = localStorage.getItem('produiiit');

       this.publicService.updateEntre({
        Produit: p, //this.Produit,
        Prix_Unitaire: this.Prix_Unitaire,
        Quantite: this.iiQuantite,
        id_users: id,
       })
       .then(data =>{
         console.log(this.Produit);
         console.log(this.Prix_Unitaire);
         console.log(this.iiQuantite);
         if(data){
          console.log('update réussi')
        }
      });
  }


}
