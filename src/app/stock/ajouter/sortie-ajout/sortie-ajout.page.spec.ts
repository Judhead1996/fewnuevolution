import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SortieAjoutPage } from './sortie-ajout.page';

describe('SortieAjoutPage', () => {
  let component: SortieAjoutPage;
  let fixture: ComponentFixture<SortieAjoutPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SortieAjoutPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SortieAjoutPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
