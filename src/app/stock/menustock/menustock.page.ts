import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-menustock',
  templateUrl: './menustock.page.html',
  styleUrls: ['./menustock.page.scss'],
})
export class MenustockPage implements OnInit {

  constructor(private router:Router, private alertController: AlertController) { }

  prenom = localStorage.getItem('nom');

  ngOnInit() {
  }

  public acc = [
    {title1:"Accueil", url:"/home", icon1:'ios-home'},
  ]

  public menus1 = [

    
    /* {title1:"Gestion stock", url:"menustock/gestionstckok", icon1:'ios-cart'},  */  
    {title1:"Historique entrées", url:"menustock/hist-entree/journalier", icon1:'ios-arrow-round-forward'},    
    {title1:"Historique sorties", url:"menustock/hist-sortie/journalier1", icon1:'ios-arrow-round-back'},    
   ]

   onMenu1Item(m){
    this.router.navigateByUrl(m.url);

  }

  public menu2 = [

    {title1:"Nous Contactez", url:"menu/nouscontacter", icono:'ios-contacts'},
   ]

   
  public menu1 = [

    {title:"A Propos", url:"menu/aprospos", icon:'ios-help-circle-outline'},  
   ]
   

  async onMenuo1Item(m){
    const alert = await this.alertController.create({
      header: 'Fewnu',
      message : " Tél: 774720737 / 782933656, <br> E-mail: contact.fewnu@gmail.com ",
        buttons : [
          {
            text: 'Ok',
          }
        ],  
    });
    alert.present();
  }

  async onMenuoItem(m){
    const alert = await this.alertController.create({
      header: 'CONCTEZ NOUS',
      
      message: "'TEL: 33867-75-08', 'PORT:78293-36-56', 'E-MAIL: contac.fewnu@gmail.com' ,'ok'",
      subHeader:' ',             
    });
    alert.present();
  }

  public logo = {
    logo : "assets/images/money.png",

  }

}
