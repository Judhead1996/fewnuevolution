import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MenustockPage } from './menustock.page';

const routes: Routes = [
  {
    path: '',
    component: MenustockPage,
    children: [
   
      { 
        path: 'accueil',
         loadChildren: '../../stock/accueil/accueil.module#AccueilPageModule'
       },

      { 
        path: 'hist-entree', 
          loadChildren: '../../stock/briefing/hist-entree/hist-entree.module#HistEntreePageModule'
       },

      { 
        path: 'hist-sortie', 
          loadChildren: '../../stock/briefing/hist-sortie/hist-sortie.module#HistSortiePageModule' 
      },


    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MenustockPage]
})
export class MenustockPageModule {}
