import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { PublicService } from 'src/app/services/data-api/public.service';

@Component({
  selector: 'app-sortie-detail',
  templateUrl: './sortie-detail.page.html',
  styleUrls: ['./sortie-detail.page.scss'],
})
export class SortieDetailPage implements OnInit {

  id = null;
  Produit = "";
  Prix_Unitaire = "";
  Quantite = "";
  Client = "";

  constructor(private  publcService:PublicService, private router:Router,
     private activatedRoute: ActivatedRoute,
     public navCtrl: NavController,
     public loadingCtrl: LoadingController, private alertCtrl: AlertController) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.Produit = this.activatedRoute.snapshot.paramMap.get('Produit');
    this.Prix_Unitaire = this.activatedRoute.snapshot.paramMap.get('Prix_Unitaire');
    this.Quantite = this.activatedRoute.snapshot.paramMap.get('Quantite');
    this.Client = this.activatedRoute.snapshot.paramMap.get('Client');

    localStorage.setItem('idSortie', this.id)
  }

  async onModif(sortie){ 
    var loader = await this.loadingCtrl.create({
      message: "Modification...",
      duration: 2000
    });
    await loader.present();

    var id = localStorage.getItem('userId'); 
       this.publcService.updateSortie({
        Produit: sortie.Produit,
        Prix_Unitaire: sortie.Prix_Unitaire,
        Quantite: sortie.Quantite,
        Client: sortie.Client,
        id_users: id,
       })
       .then(data =>{
        
         if(data){
           loader.dismiss();
          this.router.navigateByUrl('/menustock/accueil/sortie');
          console.log('update réussi')
        }else{
          this.router.navigateByUrl('/sortie-detail/');
          console.log('échec') 
        }
      });
  }

  async suppConfirm(id) {
    let alert = await this.alertCtrl.create({
      header: 'Suppression',
      message: 'Voulez vous vraiment le supprimer?',
      buttons: [
        {
          text: 'Non',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Oui',
          handler: () => {
            console.log('Buy clicked');
            this.deleteSortie(id);
          }
        }
      ]
    });
    alert.present();
  }

  async deleteSortie(id){
    var loader = await this.loadingCtrl.create({
      message: "Suupression...",
      duration: 2000
    });
    await loader.present();
    this.publcService.deleteSortie(id).then(()=>{
      loader.dismiss();
      this.router.navigateByUrl('/menustock/accueil/sortie');
    })
  }


}
