import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SortieDetailPage } from './sortie-detail.page';

describe('SortieDetailPage', () => {
  let component: SortieDetailPage;
  let fixture: ComponentFixture<SortieDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SortieDetailPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SortieDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
