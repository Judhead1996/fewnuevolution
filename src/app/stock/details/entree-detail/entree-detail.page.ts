import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController, LoadingController, AlertController } from '@ionic/angular';
import { PublicService } from 'src/app/services/data-api/public.service';


@Component({
  selector: 'app-entree-detail',
  templateUrl: './entree-detail.page.html',
  styleUrls: ['./entree-detail.page.scss'],
})
export class EntreeDetailPage implements OnInit {

  id = null;
  Produit = "";
  Prix_Unitaire = "";
  Quantite = "";

  constructor(public  publcService:PublicService, private router:Router,
     private activatedRoute: ActivatedRoute,
     public navCtrl: NavController,
    public loadingCtrl: LoadingController, private alertCtrl: AlertController) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.Produit = this.activatedRoute.snapshot.paramMap.get('Produit');
    this.Prix_Unitaire = this.activatedRoute.snapshot.paramMap.get('Prix_Unitaire');
    this.Quantite = this.activatedRoute.snapshot.paramMap.get('Quantite');
    localStorage.setItem('idEntree', this.id)
  }

  async onModif(entree){ 
    var loader = await this.loadingCtrl.create({
      message: "Modification...",
      duration: 2000
    });
    await loader.present();

    var id = localStorage.getItem('userId'); 
       this.publcService.updateEntree({
        Produit: entree.Produit,
        Prix_Unitaire: entree.Prix_Unitaire,
        Quantite: entree.Quantite,
         id_users: id,
       })
       .then(data =>{
         
         if(data){
           loader.dismiss();
          this.router.navigateByUrl('/menustock/accueil/entree');
          console.log('update réussi')
        }else{
          this.router.navigateByUrl('/entree-detail/');
          console.log('échec') 
        }
      });
  }

  async suppConfirm(id) {
    let alert = await this.alertCtrl.create({
      header: 'Suppression',
      message: 'Voulez vous vraiment le supprimer?',
      buttons: [
        {
          text: 'Non',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Oui',
          handler: () => {
            console.log('Buy clicked');
            this.deleteEntree(id);
          }
        }
      ]
    });
    alert.present();
  }

  async deleteEntree(id){
    var loader = await this.loadingCtrl.create({
      message: "Supression...",
      duration: 2000
    });
    await loader.present();
    this.publcService.deleteEntree(id).then(()=>{  
      loader.dismiss();
      this.router.navigateByUrl('/menustock/accueil/entree');
    })
  }

}
