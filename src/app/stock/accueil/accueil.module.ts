import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AccueilPage } from './accueil.page';

const routes: Routes = [
  {
    path: '',
    component: AccueilPage,
    children: [
   
      { 
        path: 'entree', 
        loadChildren: '../../stock/entree/entree.module#EntreePageModule' 
      },

      { 
        path: 'sortie',
         loadChildren: '../../stock/sortie/sortie.module#SortiePageModule'
         },

      {
        path: '',
        redirectTo: 'menustock/accueil/entree',
        pathMatch: 'full'
      }
    ],
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AccueilPage]
})
export class AccueilPageModule {}
