import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistEntreePage } from './hist-entree.page';

describe('HistEntreePage', () => {
  let component: HistEntreePage;
  let fixture: ComponentFixture<HistEntreePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistEntreePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistEntreePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
