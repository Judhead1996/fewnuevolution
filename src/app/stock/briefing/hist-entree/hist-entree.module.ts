import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HistEntreePage } from './hist-entree.page';

const routes: Routes = [
  {
    path: '',
    component: HistEntreePage,
    children: [

      { 
        path: 'journalier', 
          loadChildren: '../../../stock/briefing/journalier/journalier.module#JournalierPageModule'
       },

      { 
        path: 'mois',
         loadChildren: '../../../stock/briefing/mois/mois.module#MoisPageModule'
       },

      
       
      {
          path: '',
          redirectTo: 'menustock/hist-entree/journalier',
          pathMatch: 'full'
        }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HistEntreePage]
})
export class HistEntreePageModule {}
