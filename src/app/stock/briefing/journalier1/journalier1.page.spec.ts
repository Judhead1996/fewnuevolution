import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Journalier1Page } from './journalier1.page';

describe('Journalier1Page', () => {
  let component: Journalier1Page;
  let fixture: ComponentFixture<Journalier1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Journalier1Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Journalier1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
