import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { PublicService } from 'src/app/services/data-api/public.service';


@Component({
  selector: 'app-journalier',
  templateUrl: './journalier.page.html',
  styleUrls: ['./journalier.page.scss'],
})
export class JournalierPage implements OnInit {

  listToday = [];
  listHier = [];
  listTroisieme = [];
  listQuatrieme = [];
  listCinquieme = [];
  listSixieme = [];
  listSeptieme = [];
  listHuitieme = [];
  listNeuvieme = [];
  listDixieme = [];
  listOnzieme = [];
  listDouzieme = [];
  listTresieme = [];
  listQuatorzieme = [];
  listQuinzieme = [];
  listSezieme = [];
  listDixseptieme = [];
  listDixhuitieme = [];
  listDixneuvieme = [];
  listVingtieme = [];
  listVingtune = [];
  listVingtdeux = [];
  listVingttrois = [];
  listVingtquatre = [];
  listVingtcinq = [];
  listVingtsix = [];
  listVingtsept = [];
  listVingthuit = [];
  listVingtneuve = [];
  listTrante = [];
  listTranteun = [];



currentMois3: any;
  si4: any; si5: any; 
  si6: any; si7: any; 
  si8: any; si9: any;
  si10: any; si11: any;
  si12: any; si13: any; 
  si14: any; si15: any;
  si16: any; si17: any;
  si18: any; si19: any; 
  si20: any; si21: any; 
  si22: any; si23: any; 
  si24: any; si25: any; 
  si26: any; si27: any; si28: any; si29: any; si30: any; si31: any;

  som_1: any;
  som_2: any;
  som_3: any;
  som_4: any;
  som_5: any;
  som_6: any;
  som_7: any;
  som_8: any;
  som_9: any;
  som_10: any;
  som_11: any;
  som_12: any;
  som_13: any;
  som_14: any;
  som_15: any;
  som_16: any;
  som_17: any;
  som_18: any;
  som_19: any;
  som_20: any;
  som_21: any;
  som_22: any;
  som_23: any;
  som_24: any;
  som_25: any;
  som_26: any;
  som_27: any;
  som_28: any;
  som_29: any;
  som_30: any;
  som_31: any;

 

  formatteDate0;formatteDatea;formatteDateb;formatteDatec;formatteDated;formatteDatee;formatteDatef;
  formatteDateg;formatteDateh;formatteDatei;formatteDatej;formatteDatek;formatteDatel;formatteDatem;
  formatteDaten;formatteDateo;formatteDatep;formatteDateq;formatteDater;formatteDates;formatteDatet;
  formatteDateu;formatteDatev;formatteDatew;formatteDatex;formatteDatey;formatteDatez;

  
  formatteDate;
  formatteDate8;formatteDate9;formatteDate10;formatteDate11;formatteDate12;formatteDate13;formatteDate14;formatteDate15;
  formatteDate16;formatteDate17;formatteDate18;formatteDate19;formatteDate20;formatteDate21;formatteDate22;
  formatteDate23;formatteDate24;formatteDate25;formatteDate26;formatteDate27;formatteDate28;formatteDate29;formatteDate30;
  formatteDate1;formatteDate2;formatteDate3;formatteDate4;formatteDate5; formatteDate6;formatteDate7;

  tab1=[];tab2=[];tab3=[];tab4=[];tab5=[];tab6=[];tab7=[];tab8=[];tab9=[];tab10=[];tab11=[];tab12=[];tab13=[];tab14=[];tab15=[];
  tab16=[];tab17=[];tab18=[];tab19=[];tab20=[];tab21=[];tab22=[];tab23=[];tab24=[];tab25=[];tab26=[];
  tab27=[];tab28=[];tab29=[];tab30=[];
  jours=[];
  constructor(private publc:PublicService, public NavCtrl: NavController)  { 
    this.getFormatteDate1(),this.getFormatteDate2(),this.getFormatteDate3(),this.getFormatteDate4()
    this.getFormatteDate5(),this.getFormatteDate6(),this.getFormatteDate7(),this.getFormatteDate8()
    this.getFormatteDate9(),this.getFormatteDate10(),this.getFormatteDate11(),this.getFormatteDate12(),
    this.getFormatteDate12(),this.getFormatteDate13(),this.getFormatteDate14(),this.getFormatteDate15(),
    this.getFormatteDate16(),this.getFormatteDate17() ,this.getFormatteDate18(),this.getFormatteDate19(),
    this.getFormatteDate20(),this.getFormatteDate21()
  }

  ngOnInit() {
    this.onLoadVente(); 
  }
  
  getFormatteDate1(){
    var dateobj1 = new Date()
    dateobj1.setDate(dateobj1.getDate());
    console.log(dateobj1);
    this.formatteDate1 = dateobj1
    console.log(this.formatteDate1)
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    var formatteDate0 = jours[dateobj1.getDate()]
    this.formatteDate0 = jours[dateobj1.getDay()] + " " + dateobj1.getDate() + "-" + mois[dateobj1.getMonth()];
    this.tab1 = this.formatteDate0;  
   }

  getFormatteDate2(){
    var dateobj2 = new Date()
    dateobj2.setDate(dateobj2.getDate() -1);
    console.log(dateobj2);
    this.formatteDate2 = dateobj2
    console.log(this.formatteDate2)
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    var formatteDatea = jours[dateobj2.getDate()]
    this.formatteDatea = jours[dateobj2.getDay()] + " " + dateobj2.getDate() + "-" + mois[dateobj2.getMonth()];
    this.tab2 = this.formatteDatea; 
    
   }

  getFormatteDate3(){
    var dateobj3 = new Date()
    dateobj3.setDate(dateobj3.getDate() -2);
    console.log(dateobj3);
    this.formatteDate3 = dateobj3
    console.log(this.formatteDate3)
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    var formatteDateb = jours[dateobj3.getDate()]
    this.formatteDateb = jours[dateobj3.getDay()] + " " + dateobj3.getDate() + "-" + mois[dateobj3.getMonth()];
    this.tab3 = this.formatteDateb; 
   }

   getFormatteDate4(){
     var dateobj4 = new Date()
    dateobj4.setDate(dateobj4.getDate() -3);
    console.log(dateobj4);
    this.formatteDate4 = dateobj4
    console.log(this.formatteDate4)
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    var formatteDatec = jours[dateobj4.getDate()]
    this.formatteDatec = jours[dateobj4.getDay()] + " " + dateobj4.getDate() + "-" + mois[dateobj4.getMonth()];
    this.tab4 = this.formatteDatec; 
   }
   getFormatteDate5(){
    var dateobj5 = new Date()
    dateobj5.setDate(dateobj5.getDate() -4);
    console.log(dateobj5);
    this.formatteDate5 = dateobj5
    console.log(this.formatteDate5)
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    var formatteDated = jours[dateobj5.getDate()]
    this.formatteDated = jours[dateobj5.getDay()] + " " + dateobj5.getDate() + "-" + mois[dateobj5.getMonth()];
    this.tab5 = this.formatteDated; 
   }
   getFormatteDate6(){
    var dateobj6 = new Date()
    dateobj6.setDate(dateobj6.getDate() -5);
    console.log(dateobj6);
    this.formatteDate6 = dateobj6
    console.log(this.formatteDate6)
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    var formatteDatee = jours[dateobj6.getDate()]
    this.formatteDatee = jours[dateobj6.getDay()] + " " + dateobj6.getDate() + "-" + mois[dateobj6.getMonth()];
    this.tab6 = this.formatteDatee; 
   }
   getFormatteDate7(){
    var dateobj7 = new Date()
    dateobj7.setDate(dateobj7.getDate() -6);
    console.log(dateobj7);
    this.formatteDate7 = dateobj7
    console.log(this.formatteDate7)
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudSi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    var formatteDatef = jours[dateobj7.getDate()]
    this.formatteDatef = jours[dateobj7.getDay()] + " " + dateobj7.getDate() + "-" + mois[dateobj7.getMonth()];
    this.tab7 = this.formatteDatef; 
   }
   getFormatteDate8(){
    var dateobj8 = new Date()
    dateobj8.setDate(dateobj8.getDate() -7);
    console.log(dateobj8);
    this.formatteDate8 = dateobj8
    console.log(this.formatteDate8)
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    var formatteDateg = jours[dateobj8.getDate()]
    this.formatteDateg = jours[dateobj8.getDay()] + " " + dateobj8.getDate() + "-" + mois[dateobj8.getMonth()];
    this.tab8 = this.formatteDateg; 
   }
   getFormatteDate9(){
    var dateobj9 = new Date()
    dateobj9.setDate(dateobj9.getDate() -8);
    console.log(dateobj9);
    this.formatteDate9 = dateobj9
    console.log(this.formatteDate9)
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    var formatteDateh = jours[dateobj9.getDate()]
    this.formatteDateh = jours[dateobj9.getDay()] + " " + dateobj9.getDate() + "-" + mois[dateobj9.getMonth()];
    this.tab9 = this.formatteDateh; 
   }
   getFormatteDate10(){
    var dateobj10 = new Date()
    dateobj10.setDate(dateobj10.getDate() -9);
    console.log(dateobj10);
    this.formatteDate10 = dateobj10
    console.log(this.formatteDate10)
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    var formatteDatei = jours[dateobj10.getDate()]
    this.formatteDatei = jours[dateobj10.getDay()] + " " + dateobj10.getDate() + "-" + mois[dateobj10.getMonth()];
    this.tab10 = this.formatteDatei; 
   }
   getFormatteDate11(){
    var dateobj11 = new Date()
    dateobj11.setDate(dateobj11.getDate() -10);
    console.log(dateobj11);
    this.formatteDate11 = dateobj11
    console.log(this.formatteDate11)
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    var formatteDatej = jours[dateobj11.getDate()]
    this.formatteDatej = jours[dateobj11.getDay()] + " " + dateobj11.getDate() + "-" + mois[dateobj11.getMonth()];
    this.tab11 = this.formatteDatej; 
   }
   getFormatteDate12(){
    var dateobj12 = new Date()
    dateobj12.setDate(dateobj12.getDate() -11);
    console.log(dateobj12);
    this.formatteDate12 = dateobj12
    console.log(this.formatteDate12)
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    var formatteDatek = jours[dateobj12.getDate()]
    this.formatteDatek = jours[dateobj12.getDay()] + " " + dateobj12.getDate() + "-" + mois[dateobj12.getMonth()];
    this.tab12 = this.formatteDatek; 
   }
   getFormatteDate13(){
    var dateobj13 = new Date()
    dateobj13.setDate(dateobj13.getDate() -12);
    console.log(dateobj13);
    this.formatteDate13 = dateobj13
    console.log(this.formatteDate13)
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    var formatteDatel = jours[dateobj13.getDate()]
    this.formatteDatel = jours[dateobj13.getDay()] + " " + dateobj13.getDate() + "-" + mois[dateobj13.getMonth()];
    this.tab13 = this.formatteDatel; 
   }
   getFormatteDate14(){
    var dateobj14 = new Date()
    dateobj14.setDate(dateobj14.getDate() -13);
    console.log(dateobj14);
    this.formatteDate14 = dateobj14
    console.log(this.formatteDate14)
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    var formatteDatem = jours[dateobj14.getDate()]
    this.formatteDatem = jours[dateobj14.getDay()] + " " + dateobj14.getDate() + "-" + mois[dateobj14.getMonth()];
    this.tab14 = this.formatteDatem; 
   }
   getFormatteDate15(){
    var dateobj15 = new Date()
    dateobj15.setDate(dateobj15.getDate() -14);
    console.log(dateobj15);
    this.formatteDate15 = dateobj15
    console.log(this.formatteDate15)
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    var formatteDaten = jours[dateobj15.getDate()]
    this.formatteDaten = jours[dateobj15.getDay()] + " " + dateobj15.getDate() + "-" + mois[dateobj15.getMonth()];
    this.tab15 = this.formatteDaten; 
   }    
   getFormatteDate16(){
    var dateobj16 = new Date()
    dateobj16.setDate(dateobj16.getDate() -15);
    console.log(dateobj16);
    this.formatteDate16 = dateobj16
    console.log(this.formatteDate16)
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    var formatteDateo = jours[dateobj16.getDate()]
    this.formatteDateo = jours[dateobj16.getDay()] + " " + dateobj16.getDate() + "-" + mois[dateobj16.getMonth()];
    this.tab16 = this.formatteDateo; 
   }
   getFormatteDate17(){
    var dateobj17 = new Date()
    dateobj17.setDate(dateobj17.getDate() -16);
    console.log(dateobj17);
    this.formatteDate17 = dateobj17
    console.log(this.formatteDate17)
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    var formatteDatep = jours[dateobj17.getDate()]
    this.formatteDatep = jours[dateobj17.getDay()] + " " + dateobj17.getDate() + "-" + mois[dateobj17.getMonth()];
    this.tab17 = this.formatteDatep; 
   }
   getFormatteDate18(){
    var dateobj18 = new Date()
    dateobj18.setDate(dateobj18.getDate() -17);
    console.log(dateobj18);
    this.formatteDate18 = dateobj18
    console.log(this.formatteDate18)
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    var formatteDateq = jours[dateobj18.getDate()]
    this.formatteDateq = jours[dateobj18.getDay()] + " " + dateobj18.getDate() + "-" + mois[dateobj18.getMonth()];
    this.tab18 = this.formatteDateq; 
   }
   getFormatteDate19(){
    var dateobj19 = new Date()
    dateobj19.setDate(dateobj19.getDate() -18);
    console.log(dateobj19);
    this.formatteDate19 = dateobj19
    console.log(this.formatteDate19)
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    var formatteDater = jours[dateobj19.getDate()]
    this.formatteDater = jours[dateobj19.getDay()] + " " + dateobj19.getDate() + "-" + mois[dateobj19.getMonth()];
    this.tab19 = this.formatteDater; 
   }            
   getFormatteDate20(){
    var dateobj20 = new Date()
    dateobj20.setDate(dateobj20.getDate() -19);
    console.log(dateobj20);
    this.formatteDate20 = dateobj20
    console.log(this.formatteDate20)
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    var formatteDates = jours[dateobj20.getDate()]
    this.formatteDates = jours[dateobj20.getDay()] + " " + dateobj20.getDate() + "-" + mois[dateobj20.getMonth()];
    this.tab20 = this.formatteDates; 
   }
   getFormatteDate21(){
    var dateobj21 = new Date()
    dateobj21.setDate(dateobj21.getDate() -20);
    console.log(dateobj21);
    this.formatteDate21 = dateobj21
    console.log(this.formatteDate21)
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    var formatteDatet = jours[dateobj21.getDate()]
    this.formatteDatet = jours[dateobj21.getDay()] + " " + dateobj21.getDate() + "-" + mois[dateobj21.getMonth()];
    this.tab21= this.formatteDatet; 
   }


  myDate = new Date().toISOString();
  result : any ;
  sommeDay: any;
 

 

  onLoadVente() {
  
    this.publc.getHistoEntree()
    .then(data=>{
     
     this.result = JSON.parse(data.data);
     console.log(this.result.status)  
     this.listing();

    },err=>{
      console.log(err); 
    });
  }

  doRefresh(event) {
    console.log('Begin async operation');

    this.publc.getHistoEntree()
    .then(data=>{
     
     this.result = JSON.parse(data.data);
     console.log(this.result.status)  

     this.listing();

    });
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }

  
  listing1(){
  let finalObj = {}
   this.result.data.forEach(produit => {
    const date = produit.Date.split('T')[0]
    if (finalObj[date]) {
      finalObj[date].push(produit)
    } else {
      finalObj[date] = [produit];
    }
    
  })
  console.log(finalObj)
  }

  dateDay() {
    var tab = this.myDate.split('-');

    let date = {
      annee: tab[0],
      mois: tab[1],
      jour: tab[2].split('T')[0],
      hier: (tab[2], parseInt(tab[2]) - 1),
      troisiemeD: (parseInt(tab[2]) - 2),
      quatriemeD: (parseInt(tab[2]) - 3),
      cinquiemeD: (parseInt(tab[2]) - 4),
      sixiemeD: (parseInt(tab[2]) - 5),
      septiemeD: (parseInt(tab[2]) - 6),
      huitiemeD: (parseInt(tab[2]) - 7),
      neuviemeD: (parseInt(tab[2]) - 8),
      dixiemeD: (parseInt(tab[2]) - 9),
      onziemeD: (parseInt(tab[2]) - 10),
      douzD: (tab[2], parseInt(tab[2]) - 11),
      trezD: (parseInt(tab[2]) - 12),
      quatorzD: (parseInt(tab[2]) - 13),
      quinzD: (parseInt(tab[2]) - 14),
      sezD: (parseInt(tab[2]) - 15),
      dixseptD: (parseInt(tab[2]) - 16),
      dixhuitD: (parseInt(tab[2]) - 17),
      dixneuveD: (parseInt(tab[2]) - 18),
      vingtD: (parseInt(tab[2]) - 19),
      vingtunD: (parseInt(tab[2]) - 20),
      vingtdeuxD: (tab[2], parseInt(tab[2]) - 21),
      vingttroisD: (parseInt(tab[2]) - 22),
      vingtquatD: (parseInt(tab[2]) - 23),
      vingtcinqD: (parseInt(tab[2]) - 24),
      vingtsixD: (parseInt(tab[2]) - 25),
      vingtseptD: (parseInt(tab[2]) - 26),
      vingthuitD: (parseInt(tab[2]) - 27),
      vingtneuvD: (parseInt(tab[2]) - 28),
      tranteD: (parseInt(tab[2]) - 29),
      tranteunD: (parseInt(tab[2]) - 30),
    }

    return date;
  }

  listing() {
    let today = this.dateDay();
    
    this.result.data.forEach(produit => {

      let tab = produit.Date.split('-');
      let date = {
        annee: tab[0],
        mois: tab[1],
        jour: tab[2].split('T')[0],
      }
     

      
      let som1;
      /////////////// condition pour jour present
      if (today.annee == date.annee && today.jour == date.jour && today.mois == date.mois) {
        // this.listToday.clear(this.listToday)
        console.log(this.listToday.push(produit));
        som1 += produit.Prix;
        this.som_1 = som1;

      }

      let som2;
      /////////////// condition pour the day before
      if (today.annee == date.annee && today.hier == date.jour && today.mois == date.mois) {
        // this.listHier.clear(this.listHier)
        // this.listHier.splice(0, this.listHier.length)
        console.log("hier", this.listHier.push(produit));
        som2 += produit.Prix;
        this.som_2 = som2;
      }

      let som3;
      /////////// jour -3
      if (today.annee == date.annee && today.troisiemeD == date.jour && today.mois == date.mois) {
        // this.listTroisieme.splice(0, this.listTroisieme.length)
        console.log("jour - 3", this.listTroisieme.push(produit));
        som3 += produit.Prix;
        this.som_3 = som3;

      }

      let som4;
      /////////// jour -4
      if (today.annee == date.annee && today.quatriemeD == date.jour && today.mois == date.mois) {
        this.listQuatrieme.splice(0, this.listQuatrieme.length)
        console.log("jour -4", this.listQuatrieme.push(produit));
        som4 += produit.Prix;
        this.som_4 = som4;

      }

      let som5;
      /////////// jour -5
      if (today.annee == date.annee && today.cinquiemeD == date.jour && today.mois == date.mois) {
        this.listCinquieme.splice(0, this.listCinquieme.length)
        console.log("jour -5", this.listCinquieme.push(produit));
        som5 += produit.Prix;
        this.som_5 = som5;

      }
      
      let som6;
      /////////// jour -6
      if (today.annee == date.annee && today.sixiemeD == date.jour && today.mois == date.mois) {
        this.listSixieme.splice(0, this.listSixieme.length)
        console.log("jour -6", this.listSixieme.push(produit));
        som6 += produit.Prix;
        this.som_6 = som6;
        console.log('sidy',som6)

      }

      let som7
      /////////// jour -7
      if (today.annee == date.annee && today.septiemeD == date.jour && today.mois == date.mois) {
        this.listSeptieme.splice(0, this.listSeptieme.length)
        console.log("jour -7", this.listSeptieme.push(produit));
        som7 += produit.Prix;
        this.som_7 = som7;

      }

      let som8;
      /////////// jour -8
      if (today.annee == date.annee && today.huitiemeD == date.jour && today.mois == date.mois) {
        this.listHuitieme.splice(0, this.listHuitieme.length)
        console.log("jour -8", this.listHuitieme.push(produit));
        som8 += produit.Prix;
        this.som_8 = som8;

      }

      let som9;
      /////////// jour -9
      if (today.annee == date.annee && today.neuviemeD == date.jour && today.mois == date.mois) {
        this.listNeuvieme.splice(0, this.listNeuvieme.length)
        console.log("jour -9", this.listNeuvieme.push(produit));
        som9 += produit.Prix;
        this.som_9 = som9;

      }

      let som10;
      /////////// jour -10
      if (today.annee == date.annee && today.dixiemeD == date.jour && today.mois == date.mois) {
        this.listDixieme.splice(0, this.listDixieme.length)
        console.log("jour -10", this.listDixieme.push(produit));
        som10 += produit.Prix;
        this.som_10 = som10;

      }

      let som11;
       /////////// jour -11
       if (today.annee == date.annee && today.onziemeD == date.jour && today.mois == date.mois) {
       this.listOnzieme.splice(0, this.listOnzieme.length)
        console.log("jour -11", this.listOnzieme.push(produit));
        som11 += produit.Prix;
        this.som_11 = som11;

      }

      let som12;
       /////////// jour -12
       if (today.annee == date.annee && today.douzD == date.jour && today.mois == date.mois) {
        this.listDouzieme.splice(0, this.listDouzieme.length)
        console.log("jour -12", this.listDouzieme.push(produit));
        som12 += produit.Prix;
        this.som_12 = som12;

      }

      let som13;
       /////////// jour -13
       if (today.annee == date.annee && today.trezD == date.jour && today.mois == date.mois) {
       this.listTresieme.splice(0, this.listTresieme.length)
        console.log("jour -13", this.listTresieme.push(produit));
        som13 += produit.Prix;
        this.som_13 = som13;

      }
      
      let som14;
       /////////// jour -14
       if (today.annee == date.annee && today.quatorzD == date.jour && today.mois == date.mois) {
        this.listQuatorzieme.splice(0, this.listQuatorzieme.length)
        console.log("jour -14", this.listQuatorzieme.push(produit));
        som14 += produit.Prix;
        this.som_14 = som14;

      }

      let som15;
       /////////// jour -15
       if (today.annee == date.annee && today.quinzD == date.jour && today.mois == date.mois) {
        this.listQuinzieme.splice(0, this.listQuinzieme.length)
        console.log("jour -15", this.listQuinzieme.push(produit));
        som15 += produit.Prix;
        this.som_15 = som15;

      }

      let som16;
       /////////// jour -16
       if (today.annee == date.annee && today.sezD == date.jour && today.mois == date.mois) {
        this.listSezieme.splice(0, this.listSezieme.length)
        console.log("jour -16", this.listSezieme.push(produit));
        som16 += produit.Prix;
        this.som_16 = som16;

      }

      let som17;
       /////////// jour -17
       if (today.annee == date.annee && today.dixseptD == date.jour && today.mois == date.mois) {
        this.listDixseptieme.splice(0, this.listDixseptieme.length)
        console.log("jour -17", this.listDixseptieme.push(produit));
        som17 += produit.Prix;
        this.som_17 = som17;

      }

      let som18;
       /////////// jour -18
       if (today.annee == date.annee && today.dixhuitD == date.jour && today.mois == date.mois) {
        this.listDixhuitieme.splice(0, this.listDixhuitieme.length)
        console.log("jour -18", this.listDixhuitieme.push(produit));
        som18 += produit.Prix;
        this.som_18 = som18;

      }

      let som19;
       /////////// jour -19
       if (today.annee == date.annee && today.dixneuveD == date.jour && today.mois == date.mois) {
       this.listDixneuvieme.splice(0, this.listDixneuvieme.length)
        console.log("jour -19", this.listDixneuvieme.push(produit));
        som19 += produit.Prix;
        this.som_19 = som19;

      }

      let som20;
       /////////// jour -20
       if (today.annee == date.annee && today.vingtD == date.jour && today.mois == date.mois) {
        this.listVingtieme.splice(0, this.listVingtieme.length)
        console.log("jour -20", this.listVingtieme.push(produit));
        som20 += produit.Prix;
        this.som_20 = som20;

      }

    });
  }

}
