import { Component, OnInit } from '@angular/core';
import { PublicService } from 'src/app/services/data-api/public.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mois',
  templateUrl: './mois.page.html',
  styleUrls: ['./mois.page.scss'],
})
export class MoisPage implements OnInit {

  formatteDate; 
  
  tab1 = []; tab2 = []; tab3 = []; tab4 = []; tab5 = []; tab6 = []; tab7 = []; tab8 = []; tab9 = []; tab10 = []; tab11 = []; tab12 = []; tab13 = []; tab14 = [];tab15 = [];tab16 = [];tab17 = [];tab18 = [];tab19 = [];tab20 = [];tab21 = [];tab22 = [];tab23 = [];tab24 = [];tab25 = [];tab26 = [];tab27 = [];tab28 = [];tab29 = [];tab30 = [];tab31 = [];

  si1:any; si2:any; si3:any; 
  si4: any; si5: any; si6: any; si7: any; si8: any; si9: any; si10: any; si11: any; si12: any; si13: any; si14: any; si15: any; si16: any; si17: any; si18: any; si19: any; si20: any; si21: any; si22: any; si23: any; si24: any; si25: any; si26: any; si27: any; si28: any; si29: any; si30: any; si31: any;

  currentMois: any;
  
  constructor(private publc:PublicService, private router:Router) { 
  
 }
 myDate = new Date().toISOString();

  result : any ;
  listMoisAct = [];
  listMoisPass = [];
  listMoisPassPr = [];
  moiActu : any;
  moiPass : any;

  formatteDatecours: any;
  formatteDatepass: any;
  formatteDate1: any;
  formatteDate2: any;
  formatteDate3: any;
  formatteDate4: any;
  formatteDate5: any;
  formatteDate6: any;
  formatteDate7: any;
  formatteDate8: any;
  formatteDate9: any;
  formatteDate10: any;
  formatteDate11: any;
  formatteDate12: any;

  som_cours: any;
  som_pass: any;
  som_1: any;
  som_2: any;
  som_3: any;
  som_4: any;
  som_5: any;
  som_6: any;
  som_7: any;
  som_8: any;
  som_9: any;
  som_10: any;
  som_11: any;
  som_12: any;
  

  ngOnInit() {
    this.onLoadVente(); 
    this.getFormatteDate1();
    this.getFormatteDate2();
    this.getFormatteDate3();
    this.getFormatteDate4();
    this.getFormatteDate5();
    this.getFormatteDate6();
    this.getFormatteDate7();
    this.getFormatteDate8();
    this.getFormatteDate9();
    this.getFormatteDate10();
    this.getFormatteDate11();
    this.getFormatteDate12();
    this.getFormatteDate13();
    this.getFormatteDate14();
    this.getFormatteDate15();
    this.getFormatteDate16();
    this.getFormatteDate17();
    this.getFormatteDate18();
    this.getFormatteDate19();
    this.getFormatteDate20();
    this.getFormatteDate21();
    this.getFormatteDate22();
    this.getFormatteDate23();
    this.getFormatteDate24();
    this.getFormatteDate25();
    this.getFormatteDate26();
    this.getFormatteDate27();
    this.getFormatteDate28();
    this.getFormatteDate29();
    this.getFormatteDate30();
    this.getFormatteDate31();
  }

  onLoadVente() {
  
    this.publc.getHistoEntree()
    .then(data=>{
      this.result = JSON.parse(data.data);
      
      this.mois(); 
    
    });
  }

  doRefresh(event) {
    console.log('Begin async operation');
    this.publc.getHistoEntree()
    .then(data=>{
      this.result = JSON.parse(data.data);
      
      this.mois();
     
    });
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }

  dateDay(){
    var tab = this.myDate.split('-');
    
    let date = {
      annee : tab[0],
      mois : tab[1],
      jour : tab[2].split('T')[0],
      moisPass : (tab[1], parseInt(tab[1])-1),
      avanMoisPass : (tab[1], parseInt(tab[1])-2),
      hier: (tab[2], parseInt(tab[2]) - 1),
      troisiemeD: (parseInt(tab[2]) - 2),
      quatriemeD: (parseInt(tab[2]) - 3),
      cinquiemeD: (parseInt(tab[2]) - 4),
      sixiemeD: (parseInt(tab[2]) - 5),
      septiemeD: (parseInt(tab[2]) - 6),
      huitiemeD: (parseInt(tab[2]) - 7),
      neuviemeD: (parseInt(tab[2]) - 8),
      dixiemeD: (parseInt(tab[2]) - 9),
      onziemeD: (parseInt(tab[2]) - 10),
      douzD: (tab[2], parseInt(tab[2]) - 11),
      trezD: (parseInt(tab[2]) - 12),
      quatorzD: (parseInt(tab[2]) - 13),
      quinzD: (parseInt(tab[2]) - 14),
      sezD: (parseInt(tab[2]) - 15),
      dixseptD: (parseInt(tab[2]) - 16),
      dixhuitD: (parseInt(tab[2]) - 17),
      dixneuveD: (parseInt(tab[2]) - 18),
      vingtD: (parseInt(tab[2]) - 19),
      vingtunD: (parseInt(tab[2]) - 20),
      vingtdeuxD: (tab[2], parseInt(tab[2]) - 21),
      vingttroisD: (parseInt(tab[2]) - 22),
      vingtquatD: (parseInt(tab[2]) - 23),
      vingtcinqD: (parseInt(tab[2]) - 24),
      vingtsixD: (parseInt(tab[2]) - 25),
      vingtseptD: (parseInt(tab[2]) - 26),
      vingthuitD: (parseInt(tab[2]) - 27),
      vingtneuvD: (parseInt(tab[2]) - 28),
      tranteD: (parseInt(tab[2]) - 29),
      tranteunD: (parseInt(tab[2]) - 30),
    }   
    return date;
  }

  mois(){
    let today = this.dateDay();
    this.moiActu = today.mois;
    this.moiPass = today.moisPass;
    console.log("numero mois", this.moiActu);
    console.log("numero mois", this.moiPass);

    
   this.result.data.forEach(produit => {
     
     let tab = produit.Date.split('-');
     let date = {
       annee : tab[0],
       mois : tab[1],
       jour : tab[2].split('T')[0],
     }
     
     /////////////// condition pour mois sen cours
     if (today.annee == date.annee && today.mois == date.mois) {
      
       this.som_cours += produit.Prix;
       this.formatteDatecours = produit.Date

     }

     
     /////////////// condition pour mois passé
     if (today.annee == date.annee && today.moisPass == date.mois) {

       this.som_pass += produit.Prix;
       this.formatteDatepass = produit.Date

     }

      /////////////// decembre
      if (today.annee == date.annee && 12 == date.mois) {
        
        this.som_12 += produit.Prix;
        this.formatteDate12 = produit.Date
 
      }

      /////////////// novembre
      if (today.annee == date.annee && 11 == date.mois) {
      
        this.som_11 += produit.Prix;
        this.formatteDate11 = produit.Date
 
      }

      /////////////// octobre
      if (today.annee == date.annee && 10 == date.mois) {
      
        this.som_10 += produit.Prix;
        this.formatteDate10 = produit.Date
 
      }
    
      /////////////// septembre
      if (today.annee == date.annee && 9 == date.mois) {
      
        this.som_9 += produit.Prix;
        this.formatteDate9 = produit.Date
 
      }

       /////////////// aout
       if (today.annee == date.annee && 8 == date.mois) {
      
        this.som_8 += produit.Prix;
        this.formatteDate8 = produit.Date
 
      }

       /////////////// juillet
       if (today.annee == date.annee && 7 == date.mois) {
      
        this.som_7 += produit.Prix;
        this.formatteDate7 = produit.Date
 
      }

       /////////////// juin
       if (today.annee == date.annee && 6 == date.mois) {
      
        this.som_6 += produit.Prix;
        this.formatteDate6 = produit.Date
 
      }

       /////////////// mai
       if (today.annee == date.annee && 5 == date.mois) {
      
        this.som_5 += produit.Prix;
        this.formatteDate5 = produit.Date
 
      }

       /////////////// avril
       if (today.annee == date.annee && 4 == date.mois) {
      
        this.som_4 += produit.Prix;
        this.formatteDate4 = produit.Date
 
      }

       /////////////// mars
       if (today.annee == date.annee && 3 == date.mois) {
      
        this.som_3 += produit.Prix;
        this.formatteDate3 = produit.Date
 
      }

      /////////////// fevrier
      if (today.annee == date.annee && 2 == date.mois) {
      
        this.som_2 += produit.Prix;
        this.formatteDate2 = produit.Date
 
      }

       /////////////// janvier
       if (today.annee == date.annee && 1 == date.mois) {
      
        this.som_1 += produit.Prix;
        this.formatteDate1 = produit.Date
 
      }

   });


 }

 getFormatteDate1() {
  let dateobj1 = new Date();
  dateobj1.setDate(dateobj1.getDate());
  var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
  var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
  this.formatteDate = jours[dateobj1.getDay()] + " " + dateobj1.getDate() + " " + mois[dateobj1.getMonth()];
  this.tab1 = this.formatteDate;

  console.log("date first" , this.tab1)

   this.si1 = mois[dateobj1.getMonth()];

   console.log('56 = ', this.si1)
}

 getFormatteDate2() {
  let dateobj2 = new Date();
  dateobj2.setDate(dateobj2.getDate() - 1);
  var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
  var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
  this.formatteDate = jours[dateobj2.getDay()] + " " + dateobj2.getDate() + " " + mois[dateobj2.getMonth()];
  this.tab2 = this.formatteDate;

  // this.currentMois3 = mois[dateobj3.getMonth()];
}

 getFormatteDate3() {
  let dateobj3 = new Date();
  dateobj3.setDate(dateobj3.getDate() - 2);
  var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
  var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
  this.formatteDate = jours[dateobj3.getDay()] + " " + dateobj3.getDate() + " " + mois[dateobj3.getMonth()];
  this.tab3 = this.formatteDate;

  // this.currentMois3 = mois[dateobj3.getMonth()];
}
  
getFormatteDate4() {
  var dateobj4 = new Date()
  dateobj4.setDate(dateobj4.getDate() - 3);
  console.log(dateobj4);
  var jours = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];
  var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
  this.formatteDate = jours[dateobj4.getDay()] + " " + dateobj4.getDate() + " " + mois[dateobj4.getMonth()];
  this.tab4 = this.formatteDate;

  // this.si4 = mois[dateobj4.getMonth()];
}

getFormatteDate5() {
  var dateobj5 = new Date()
  dateobj5.setDate(dateobj5.getDate() - 4);
  this.formatteDate = dateobj5
  var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
  var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
  this.formatteDate = jours[dateobj5.getDay()] + " " + dateobj5.getDate() + " " + mois[dateobj5.getMonth()];
  this.tab5 = this.formatteDate;

  // this.si5 = mois[dateobj5.getMonth()];
}

getFormatteDate6() {
  var dateobj6 = new Date()
  dateobj6.setDate(dateobj6.getDate() - 5);
  this.formatteDate = dateobj6
  var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
  var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
  this.formatteDate = jours[dateobj6.getDay()] + " " + dateobj6.getDate() + " " + mois[dateobj6.getMonth()];
  this.tab6 = this.formatteDate;

  // this.si6 = mois[dateobj6.getMonth()];
}

getFormatteDate7() {
  var dateobj7 = new Date()
  dateobj7.setDate(dateobj7.getDate() - 6);
  var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
  var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
  this.formatteDate = jours[dateobj7.getDay()] + " " + dateobj7.getDate() + " " + mois[dateobj7.getMonth()];
  this.tab7 = this.formatteDate;

  // this.si7 = mois[dateobj7.getMonth()];
}

getFormatteDate8() {
  var dateobj8 = new Date()
  dateobj8.setDate(dateobj8.getDate() - 7);
  var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
  var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
  this.formatteDate = jours[dateobj8.getDay()] + " " + dateobj8.getDate() + " " + mois[dateobj8.getMonth()];
  this.tab8 = this.formatteDate;

  // this.si8 = mois[dateobj8.getMonth()];
}

getFormatteDate9() {
  var dateobj9 = new Date()
  dateobj9.setDate(dateobj9.getDate() - 8);  
  var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
  var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
  this.formatteDate = jours[dateobj9.getDay()] + " " + dateobj9.getDate() + " " + mois[dateobj9.getMonth()];
  this.tab9 = this.formatteDate;

  // this.si9 = mois[dateobj9.getMonth()];
}

getFormatteDate10() {
  var dateobj10 = new Date()
  dateobj10.setDate(dateobj10.getDate() - 9);
  var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
  var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
  this.formatteDate = jours[dateobj10.getDay()] + " " + dateobj10.getDate() + " " + mois[dateobj10.getMonth()];
  this.tab10 = this.formatteDate;

  // this.si10 = mois[dateobj10.getMonth()];
}

getFormatteDate11() {
  var dateobj11 = new Date()
  dateobj11.setDate(dateobj11.getDate() - 10);
  var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
  var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
  this.formatteDate = jours[dateobj11.getDay()] + " " + dateobj11.getDate() + " " + mois[dateobj11.getMonth()];
  this.tab11 = this.formatteDate;

 // this.si11 = mois[dateobj11.getMonth()];
}

getFormatteDate12() {
  var dateobj12 = new Date()
  dateobj12.setDate(dateobj12.getDate() - 11);
  var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
  var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
  this.formatteDate = jours[dateobj12.getDay()] + " " + dateobj12.getDate() + " " + mois[dateobj12.getMonth()];
  this.tab12 = this.formatteDate;

  // this.si12 = mois[dateobj12.getMonth()];
}

getFormatteDate13() {
  var dateobj13 = new Date()
  dateobj13.setDate(dateobj13.getDate() - 12);
  var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
  var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
  this.formatteDate = jours[dateobj13.getDay()] + " " + dateobj13.getDate() + " " + mois[dateobj13.getMonth()];
  this.tab13 = this.formatteDate;

 // this.si13 = mois[dateobj13.getMonth()];
}

getFormatteDate14() {
  var dateobj14 = new Date()
  dateobj14.setDate(dateobj14.getDate() - 13);
  var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
  var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
  this.formatteDate = jours[dateobj14.getDay()] + " " + dateobj14.getDate() + " " + mois[dateobj14.getMonth()];
  this.tab14 = this.formatteDate;

  // this.si14 = mois[dateobj14.getMonth()];
}

getFormatteDate15() {
  var dateobj15 = new Date()
  dateobj15.setDate(dateobj15.getDate() - 14);
  var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
  var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
  this.formatteDate = jours[dateobj15.getDay()] + " " + dateobj15.getDate() + " " + mois[dateobj15.getMonth()];
  this.tab15 = this.formatteDate;

  // this.si15 = mois[dateobj15.getMonth()];
}

getFormatteDate16() {
  var dateobj16 = new Date()
  dateobj16.setDate(dateobj16.getDate() - 15);
  var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
  var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
  this.formatteDate = jours[dateobj16.getDay()] + " " + dateobj16.getDate() + " " + mois[dateobj16.getMonth()];
  this.tab16 = this.formatteDate;

 // this.si16 = mois[dateobj16.getMonth()];
}

getFormatteDate17() {
  var dateobj17 = new Date()
  dateobj17.setDate(dateobj17.getDate() - 16);
  var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
  var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
  this.formatteDate = jours[dateobj17.getDay()] + " " + dateobj17.getDate() + " " + mois[dateobj17.getMonth()];
  this.tab17 = this.formatteDate;

  // this.si17 = mois[dateobj17.getMonth()];
}

getFormatteDate18() {
  var dateobj18 = new Date()
  dateobj18.setDate(dateobj18.getDate() - 17);
  var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
  var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
  this.formatteDate = jours[dateobj18.getDay()] + " " + dateobj18.getDate() + " " + mois[dateobj18.getMonth()];
  this.tab18 = this.formatteDate;

  // this.si18 = mois[dateobj18.getMonth()];
}

getFormatteDate19() {
  var dateobj19 = new Date()
  dateobj19.setDate(dateobj19.getDate() - 18);
  var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
  var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
  this.formatteDate = jours[dateobj19.getDay()] + " " + dateobj19.getDate() + " " + mois[dateobj19.getMonth()];
  this.tab19 = this.formatteDate;

  // this.si19 = mois[dateobj19.getMonth()];
}

getFormatteDate20() {
  var dateobj20 = new Date()
  dateobj20.setDate(dateobj20.getDate() - 19);
  var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
  var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
  this.formatteDate = jours[dateobj20.getDay()] + " " + dateobj20.getDate() + " " + mois[dateobj20.getMonth()];
  this.tab20 = this.formatteDate;

 // this.si20 = mois[dateobj20.getMonth()];
}

getFormatteDate21() {
  var dateobj21 = new Date()
  dateobj21.setDate(dateobj21.getDate() - 20);
  var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
  var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
  this.formatteDate = jours[dateobj21.getDay()] + " " + dateobj21.getDate() + " " + mois[dateobj21.getMonth()];
  this.tab21 = this.formatteDate;

 // this.si21 = mois[dateobj21.getMonth()];
}

getFormatteDate22() {
  var dateobj22 = new Date()
  dateobj22.setDate(dateobj22.getDate() - 21);
  var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
  var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
  this.formatteDate = jours[dateobj22.getDay()] + " " + dateobj22.getDate() + " " + mois[dateobj22.getMonth()];
  this.tab22 = this.formatteDate;

  // this.si22 = mois[dateobj22.getMonth()];
}

getFormatteDate23() {
  var dateobj23 = new Date()
  dateobj23.setDate(dateobj23.getDate() - 22);
  var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
  var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
  this.formatteDate = jours[dateobj23.getDay()] + " " + dateobj23.getDate() + " " + mois[dateobj23.getMonth()];
  this.tab23 = this.formatteDate;

 // this.si23 = mois[dateobj23.getMonth()];
}

getFormatteDate24() {
  var dateobj24 = new Date()
  dateobj24.setDate(dateobj24.getDate() - 23);
  var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
  var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
  this.formatteDate = jours[dateobj24.getDay()] + " " + dateobj24.getDate() + " " + mois[dateobj24.getMonth()];
  this.tab24 = this.formatteDate;

 // this.si24 = mois[dateobj24.getMonth()];
}

getFormatteDate25() {
  var dateobj25 = new Date()
  dateobj25.setDate(dateobj25.getDate() - 24);
  var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
  var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
  this.formatteDate = jours[dateobj25.getDay()] + " " + dateobj25.getDate() + " " + mois[dateobj25.getMonth()];
  this.tab25 = this.formatteDate;

 // this.si25 = mois[dateobj25.getMonth()];
}

getFormatteDate26() {
  var dateobj26 = new Date()
  dateobj26.setDate(dateobj26.getDate() - 25);
  var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
  var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
  this.formatteDate = jours[dateobj26.getDay()] + " " + dateobj26.getDate() + " " + mois[dateobj26.getMonth()];
  this.tab26 = this.formatteDate;

 // this.si26 = mois[dateobj26.getMonth()];
}

getFormatteDate27() {
  var dateobj27 = new Date()
  dateobj27.setDate(dateobj27.getDate() - 26);
  var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
  var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
  this.formatteDate = jours[dateobj27.getDay()] + " " + dateobj27.getDate() + " " + mois[dateobj27.getMonth()];
  this.tab27 = this.formatteDate;

  // this.si27 = mois[dateobj27.getMonth()];
}

getFormatteDate28() {
  var dateobj28 = new Date()
  dateobj28.setDate(dateobj28.getDate() - 27);
  var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
  var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
  this.formatteDate = jours[dateobj28.getDay()] + " " + dateobj28.getDate() + " " + mois[dateobj28.getMonth()];
  this.tab28 = this.formatteDate;

 // this.si28 = mois[dateobj28.getMonth()];
}

getFormatteDate29() {
  var dateobj29 = new Date()
  dateobj29.setDate(dateobj29.getDate() - 28);
  var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
  var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
  this.formatteDate = jours[dateobj29.getDay()] + " " + dateobj29.getDate() + " " + mois[dateobj29.getMonth()];
  this.tab29 = this.formatteDate;

  this.si29 = mois[dateobj29.getMonth()];
}

getFormatteDate30() {
  var dateobj30 = new Date()
  dateobj30.setDate(dateobj30.getDate() - 29);
  var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
  var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
  this.formatteDate = jours[dateobj30.getDay()] + " " + dateobj30.getDate() + " " + mois[dateobj30.getMonth()];
  this.tab30 = this.formatteDate;

  this.si30 = mois[dateobj30.getMonth()];
  console.log('2 = ', this.si30)
}

getFormatteDate31() {
  var dateobj31 = new Date()
  dateobj31.setDate(dateobj31.getDate() - 30);
  var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
  var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'Aout', 'septembre', 'octobre', 'novembre', 'decembre'];
  this.formatteDate = jours[dateobj31.getDay()] + " " + dateobj31.getDate() + " " + mois[dateobj31.getMonth()];
  this.tab31 = this.formatteDate;

    this.currentMois = mois[dateobj31.getMonth()];
    console.log('1 = ', this.currentMois)
}

}
