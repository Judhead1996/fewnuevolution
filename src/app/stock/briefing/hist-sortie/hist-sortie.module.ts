import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HistSortiePage } from './hist-sortie.page';

const routes: Routes = [
  {
    path: '',
    component: HistSortiePage,
    children: [

      { 
        path: 'journalier1',
         loadChildren: '../../../stock/briefing/journalier1/journalier1.module#Journalier1PageModule'
       },

      { 
        path: 'mois1', 
          loadChildren: '../../../stock/briefing/mois1/mois1.module#Mois1PageModule' 
        },

      
       
      {
          path: '',
          redirectTo: 'menustock/hist-sortie/journalier1',
          pathMatch: 'full'
        }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HistSortiePage]
})
export class HistSortiePageModule {}
