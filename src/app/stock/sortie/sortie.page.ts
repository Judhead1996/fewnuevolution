import { Component, OnInit } from '@angular/core';
import { PublicService } from 'src/app/services/data-api/public.service';
import { LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-sortie',
  templateUrl: './sortie.page.html',
  styleUrls: ['./sortie.page.scss'],
})
export class SortiePage implements OnInit {

  constructor(private publc:PublicService, public loadingCtrl: LoadingController,
    private storage: Storage) { }

  key: string = 'sortie';

  result : any ;
  somming : any ;

  ngOnInit() {
    this.onLoadSortie();
    this.loadData();
  }

  async onLoadSortie() {
    var loader = await this.loadingCtrl.create({
      message: "Please Wait...",
      duration: 2000
    });
    await loader.present();

    this.publc.getSortie()
    .then(data=>{
      loader.dismiss();

     this.result = JSON.parse(data.data);
     console.log(this.result.status);
     this.somming = this.total(); 
     
    });
  }

  doRefresh(event) {
    console.log('Begin async operation');

    this.publc.getSortie()
    .then(data=>{
     
     this.result = JSON.parse(data.data);
     console.log(this.result.status);
      this.somming = this.total(); 
     
    });
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }    

   ////////

   loadData(){
    this.storage.get(this.key).then((val) =>{
      if(val!=null && val != undefined){
        this.result = JSON.parse(val);
        this.somming = this.total();
      }
    })
  }

  /* somme(){
   
    let somm = 0;
    let t = 0;

    this.result.data.forEach(produit => {
    
    somm = somm + produit.Prix_Unitaire 
    t = t + parseInt(produit.Quantite)
      
    this.somming = somm * t;
    console.log("Prix unitaire ", somm);
    console.log("Quantite ", t);

    console.log("Total ", this.somming);

   });

  }*/

  total() {
    let s = 0;
    for (let i = 0; i < this.result.data.length; i++) {
      s = s + this.result.data[i].Prix_Unitaire * parseInt(this.result.data[i].Quantite);

    }
    return s;
  }

}
