import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SortiePage } from './sortie.page';

describe('SortiePage', () => {
  let component: SortiePage;
  let fixture: ComponentFixture<SortiePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SortiePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SortiePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
