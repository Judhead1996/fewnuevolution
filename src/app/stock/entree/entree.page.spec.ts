import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntreePage } from './entree.page';

describe('EntreePage', () => {
  let component: EntreePage;
  let fixture: ComponentFixture<EntreePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntreePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntreePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
