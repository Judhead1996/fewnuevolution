import { Component, OnInit } from '@angular/core';
import { PublicService } from 'src/app/services/data-api/public.service';
import { LoadingController, AlertController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-entree',
  templateUrl: './entree.page.html',
  styleUrls: ['./entree.page.scss'],
})
export class EntreePage implements OnInit {

  constructor(private publc:PublicService, 
    public loadingCtrl: LoadingController, private alertCtrl: AlertController,
    private toastController: ToastController,
    private storage: Storage) { }

    key: string = 'entree';

  result : any ;
  somming : any;

  ngOnInit() {
    this.onLoadEntree();
    this.loadData();
  }

  async onLoadEntree() {
    var loader = await this.loadingCtrl.create({
      message: "Please Wait...",
      duration: 2000
    });
    await loader.present();

    this.publc.getEntree()
    .then(data=>{
      loader.dismiss();
     this.result = JSON.parse(data.data);
     console.log(this.result.status)  
     // this.somming = this.somme();
      this.somming = this.total() 
    });
  }

  doRefresh(event) {
    console.log('Begin async operation');

    this.publc.getEntree()
    .then(data=>{
     
     this.result = JSON.parse(data.data);
     console.log(this.result.status)  
     // this.somming = this.somme();
     this.somming = this.total() 
    });
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }

   ////////

   loadData(){
    this.storage.get(this.key).then((val) =>{
      if(val!=null && val != undefined){
        this.result = JSON.parse(val);

        this.somming = this.total()
      }
    })
  }

  somme(){
   
    let somm = 0;
    let t = 0;
    let fin = 0;
    this.result.data.forEach(produit => {
    
    somm = somm + produit.Prix_Unitaire 
    t = t + parseInt(produit.Quantite)
      
    fin = somm * t;
    console.log("Prix unitaire ", somm);
    console.log("Quantite ", t);

   

   });

   return fin;
  }

  total() {
    let s = 0;
    for (let i = 0; i < this.result.data.length; i++) {
      s = s + this.result.data[i].Prix_Unitaire * parseInt(this.result.data[i].Quantite);

    }
    return s;
  }

  somQuantite() {
    let s = 0;
    for (let i = 0; i < this.result.data.length; i++) {
      s = s + this.result.data[i].Quantite;

    }
    return s;
  }

  async ajoutPrompt() {
    let alert = await this.alertCtrl.create({
      header: 'Ajouter une entrée',
      inputs: [
        {
          name: 'Produit',
          placeholder: 'Produit'
        },
        {
          name: 'Prix_Unitaire',
          placeholder: 'Prix unitaire'
        },
        {
          name: 'Quantite',
          placeholder: 'Quantité'
        }, 
      ],
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Ok',
          handler: data => {
           
            if (data.Produit, data.Prix_Unitaire, data.Quantite) {

                let id = localStorage.getItem('userId');

                this.publc.ajoutentree({
                  Produit: data.Produit,
                  Prix_Unitaire: data.Prix_Unitaire,
                  Quantite: data.Quantite,
                  id_users: id,
                }).then(data => {
      
                this.showToast('réussi');
                this.onLoadEntree();
              });

              this.publc.ajoutHistoEntree({
                Produit: data.Produit,
                Prix_Unitaire: data.Prix_Unitaire,
                Quantite: data.Quantite,
                id_users: id,
              }) 

            } else {
              
              return false;
            } 
          }
        }
      ]
    });
    alert.present();
  }

    // Helper
    async showToast(msg){
      const toast = await this.toastController.create({
        message: msg,
        duration: 2000
      });
      toast.present();
    }

}
