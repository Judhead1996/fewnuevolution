import { Component, OnInit } from '@angular/core';
import { LoadingController, ToastController } from '@ionic/angular';
import { StorageService } from '../services/storage/storage.service';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  constructor(public loadingCtrl: LoadingController,
    private storg: StorageService,
    private toastController: ToastController
    ) {}

  ngOnInit() {
    this.waiting()
  }

  async waiting(){
    var loader = await this.loadingCtrl.create({
      message: "Chargement des données...",
      duration: 15000
    });
    await loader.present();

    this. getVenteJson();
    this. getDepenseJson();
    this. getPretJson();
    this. getEntreeJson();
    this. getSortieJson();

    this.showToast('Enregistrement réussi');

    loader.dismiss();
  }

  getVenteJson(){
    this.storg.getDataVente();
  }

  getDepenseJson(){
    this.storg.getDataDepense();
  }

  getPretJson(){
    this.storg.getDataPret();
  }

  getEntreeJson(){
    this.storg.getDataEntree();
  }

  getSortieJson(){
    this.storg.getDataSortie();
  }

   // Helper
   async showToast(msg){
    const toast = await this.toastController.create({
      message: msg,
      duration: 4000
    });
    toast.present();
  }

}
