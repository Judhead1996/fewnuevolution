import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { PublicService } from '../data-api/public.service';


@Injectable({
  providedIn: 'root'
})
export class StorageService {

  keyv: string = 'vente';

  keyd: string = 'depense';

  keyp: string = 'pret';

  keyentree: string = 'entree';

  keysortie: string = 'sortie';

  result: any;

  constructor(private publc: PublicService, private storage: Storage) { }

  getDataVente(){
    this.publc.getVente()
    .then(data => {
     
      this.result = JSON.parse(data.data);
     /////// save
    /// localStorage.setItem('userId', phone )
      this.storage.set(this.keyv, JSON.stringify(this.result))
      console.log('les vente', this.result)

    })
  }  

  getDataDepense(){
    this.publc.getDepense()
    .then(data => {
     
      this.result = JSON.parse(data.data);
     
      this.storage.set(this.keyd, JSON.stringify(this.result))
      console.log('les dépenses', this.result)

    })
  }  

  getDataPret(){
    this.publc.getPret()
    .then(data => {
     
      this.result = JSON.parse(data.data);
     
      this.storage.set(this.keyp, JSON.stringify(this.result))
      console.log('the vie', this.result)

    })
  }  

  getDataEntree(){
    this.publc.getEntree()
    .then(data => {
     
      this.result = JSON.parse(data.data);
     
      this.storage.set(this.keyentree, JSON.stringify(this.result))
      console.log('the vie', this.result)

    })
  }  

  getDataSortie(){
    this.publc.getSortie()
    .then(data => {
     
      this.result = JSON.parse(data.data);
     
      this.storage.set(this.keysortie, JSON.stringify(this.result))
      console.log('the vie', this.result)

    })
  }  

  /*
  saveDataVente(){
      this.storage.set(this.key, JSON.stringify(this.result));  
  }
  */

  /*
  loadData(){
    this.storage.get(this.key).then((val) =>{
      if(val!=null && val != undefined){
        this.result = JSON.parse(val);
      }
    })
  }
  */
  
}
