import { Injectable } from '@angular/core';
import {rootUrl} from "../../url-api/server-url.js";
import { HTTP } from '@ionic-native/http/ngx';

@Injectable({
  providedIn: 'root'
})
export class PublicService {

  constructor(private http: HTTP) { }

  getVente() {
    var id = localStorage.getItem('userId');
    return this.http.get(rootUrl + "/api/vente/"+id+"/", {}, {});
  }

  getDepense() {
    var id = localStorage.getItem('userId');
    return this.http.get(rootUrl + "/api/depense/"+id+"/", {}, {});
  }

  getPret() {
    var id = localStorage.getItem('userId');
    return this.http.get(rootUrl + "/api/pret/"+id+"/", {}, {});
  }

  getEntree() {
    var id = localStorage.getItem('userId');
    return this.http.get(rootUrl + "/api/Entree/"+id+"/", {}, {});
  }

  getHistoEntree() {
    var id = localStorage.getItem('userId');
    return this.http.get(rootUrl + "/api/historiqueentree/"+id+"/", {}, {});
  }

  getSortie() { 
    var id = localStorage.getItem('userId'); 
    return this.http.get(rootUrl + "/api/Sortie/"+id+"/", {}, {});
  }

  getHistoSortie() {
    var id = localStorage.getItem('userId');
    return this.http.get(rootUrl + "/api/historiquesortie/"+id+"/", {}, {});
  }

  getHistoPret() {
    var id = localStorage.getItem('userId');
    return this.http.get(rootUrl + "/api/historiquepret/"+id+"/", {}, {});
  }
  ///////////////////////////////////////////////////////////////////////////////

  deleteVente(id) {
    return this.http.delete(rootUrl + `/api/ventedelete/${id}/`, {}, {})
  }

  updateVente(vente) {
    var id = localStorage.getItem('idVente');
    return this.http.patch(rootUrl + "/api/venteupdate/"+id+"/", vente, {})
  }

  deleteDepense(id) {
    return this.http.delete(rootUrl + `/api/depensedelete/${id}/`, {}, {})
  }

  updateDepense(depense) {
    var id = localStorage.getItem('idDepense');
    return this.http.patch(rootUrl + "/api/depenseupdate/"+id+"/", depense, {})
  }

  deletePret(id) {
    return this.http.delete(rootUrl + `/api/pretdelete/${id}/`, {}, {})
  }

  updatePret(pret) {
    var id = localStorage.getItem('idPret');
    return this.http.patch(rootUrl + "/api/pretupdate/"+id+"/", pret, {})
  }

  ///////////////////////////////////////////////////////////////////////////////
  ajoutvente(vente) {
    var id = localStorage.getItem('userId');
    return this.http.post(rootUrl + "/api/vente/"+id+"/", vente, {});
  }

  ajoutdepense(depense) {
    var id = localStorage.getItem('userId');
    return this.http.post(rootUrl + "/api/depense/"+id+"/", depense, {});
  }

  ajoutpret(pret) {
    var id = localStorage.getItem('userId');
    return this.http.post(rootUrl + "/api/pret/"+id+"/", pret, {});
  }

  ajoutentree(entre) {
    var id = localStorage.getItem('userId');
    return this.http.post(rootUrl + "/api/Entree/"+id+"/", entre, {});
  }

  ajoutHistoEntree(entre) {
    var id = localStorage.getItem('userId');
    return this.http.post(rootUrl + "/api/historiqueentree/"+id+"/", entre, {});
  }

  ajoutsortie(sorti) {
    var id = localStorage.getItem('userId');
    return this.http.post(rootUrl + "/api/historiquesortie/"+id+ "/", sorti, {});
  }

  ajoutHistoSortie(sorti) {
    var id = localStorage.getItem('userId');
    return this.http.post(rootUrl + "/api/Sortie/"+id+ "/", sorti, {});
  }

  ajoutHistoPret(sorti) {
    var id = localStorage.getItem('userId');
    return this.http.post(rootUrl + "/api/historiquepret/"+id+ "/", sorti, {}); 
  }

  ///////////////////////////////////////////////////////////////////////////////////////////

  deleteEntree(id) {
    return this.http.delete(rootUrl + `/api/deleteentree/${id}/`, {}, {})
  }

  updateEntree(entree) {
    var id = localStorage.getItem('idEntree');
    return this.http.patch(rootUrl + "/api/updateentree/"+id+"/", entree, {})
  }

  updateEntre(entre) {
    var id = localStorage.getItem('idSorti');
    return this.http.patch(rootUrl + "/api/updateentree/"+id+"/", entre, {})
  }

  deleteSortie(id) {
    return this.http.delete(rootUrl + `/api/deletesortie/${id}/`, {}, {})
  }

  updateSortie(sortie) {
    var id = localStorage.getItem('idSortie');
    return this.http.patch(rootUrl + "/api/updatesortie/"+id+"/", sortie, {})
  }

}
