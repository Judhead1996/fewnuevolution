import { Injectable } from '@angular/core';
import {rootUrl} from "../../url-api/server-url.js";
import { HTTP } from '@ionic-native/http/ngx';



@Injectable({
  providedIn: 'root'
})
export class AuthService {
  
  constructor(private httpp:HTTP) { }

  createUser(user){
    return this.httpp.post(rootUrl+'/api/registeruser/', user, {});
  }

   login(user){
    return this.httpp.post(rootUrl+'/api/loginuser/', user, {});
  }
 
  logout() {
    localStorage.removeItem('userId');
  }

}
