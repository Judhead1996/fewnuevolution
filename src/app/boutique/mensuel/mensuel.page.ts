import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mensuel',
  templateUrl: './mensuel.page.html',
  styleUrls: ['./mensuel.page.scss'],
})
export class MensuelPage implements OnInit {

  myDate = new Date().toISOString();

  totalvM : any ;
  totaldM : any; 

  constructor() { }

  ngOnInit() {
    this. doRefresh(event);
  }

  doRefresh(event) {
    console.log('Begin async operation');
    var tab = this.myDate.split('-');
    console.log(tab)
    let date = {
      annee : tab[0],
      mois : tab[1],
      jour : tab[2].split('T')[0],
    }
     
    console.log(date)

    this.totalvM = localStorage.getItem('totalVenteMois');
   
    
    this.totaldM = localStorage.getItem('totalDepenseMois');;
    
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }
  


}
