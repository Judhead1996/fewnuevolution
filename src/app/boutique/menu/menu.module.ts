import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: '',
    component: MenuPage,
    children: [

      { path: 'accueil', 
        loadChildren: '../../boutique/accueil/accueil.module#AccueilPageModule'
       },

      { path: 'journalier', 
        loadChildren: '../../boutique/journalier/journalier.module#JournalierPageModule' },

      { path: 'hebdomadaire',
       loadChildren: '../../boutique/hebdomadaire/hebdomadaire.module#HebdomadairePageModule' },

      { path: 'mensuel',
       loadChildren: '../../boutique/mensuel/mensuel.module#MensuelPageModule' },

      { path: 'inventaires',
       loadChildren: '../../boutique/inventaires/inventaires.module#InventairesPageModule' },

    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MenuPage]
})
export class MenuPageModule {}
