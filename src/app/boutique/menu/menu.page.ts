import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  private selectedItem: any;
    
    prenom = localStorage.getItem('nom');

    public acc = [
      {title:"Accueil", url:"/home", icon:'ios-home'},
    ]

    public menus = [

      
      {title:"Journalier", url:"menu/journalier", icon:'md-list-box'},
      {title:"Hebdomadaire", url:"menu/hebdomadaire", icon:'ios-calendar'},
      {title:"Mensuel", url:"menu/mensuel", icon:'md-calendar'},
      {title:"Inventaires", url:"menu/inventaires/journalier-inv", icon:'ios-calculator'},
      
      
    ]

    public men = [
      {titl:"Marges Benefice", url:"menu/benefice", ico:'ios-pulse'},
    ]

    constructor(private router:Router, private alertController: AlertController) { }

    ngOnInit() {
      
    }


    async onMenoItem(m) {
      const alert = await this.alertController.create({
        header: 'Marge de Bénéfice',
        inputs: [
          {
            name: 'checkbox1',
            type: 'radio',
            label: 'Enlever la marge',
            value: 'value1',
            checked: true
          },
          {
            name: 'checkbox2',
            type: 'radio',
            label: '+ 10%',
            value: 'value2',
            
          },
    
          {
            name: 'checkbox3',
            type: 'radio',
            label: '+ 20%',
            value: 'value3'
          },
    
          {
            name: 'checkbox4',
            type: 'radio',
            label: '+ 30%',
            value: 'value4'
          },
    
        ],
        buttons: [
          {
            text: 'Retour',
            role: 'cancel',
          },
          {
            text: 'Valider',
    
          }
          
        ]
      });
    
      await alert.present();
    }

    
    public menu2 = [

      {title1:"Nous Contactez", url:"menu/nouscontacter", icono:'ios-contacts'},
    ]

    
    public menu1 = [

      {title:"A Propos", url:"menu/aprospos", icon:'ios-help-circle-outline'},  
    ]
    

    async onMenuo1Item(m){
      const alert = await this.alertController.create({
        header: 'Fewnu',
        message:"Fewnu est une application mobile de Gestion de boutique ou de business qui fonctionne sans connexion internet. Elle vous permet d'enregistrer vos ventes, vos dépenses et vos prêts quotidiens, de faire le résumé journalier, hebdomadaire et mensuel de vos ventes et dépenses ainsi que l'inventaire   L'application Fewnu vous permet de gérer aussi votre stock c'est à dire les entrées et les sorties de votre boutique.Avec Fewnu tenez votre boutique en main!!",
        buttons : [
          {
            text: 'Ok',
          }
        ],
      });
      alert.present();
    }

    async onMenuoItem(m){
      const alert = await this.alertController.create({
        header: 'CONTACTEZ NOUS',
        message : " Tél: 774720737 / 782933656, <br> E-mail: contact.fewnu@gmail.com ",
        buttons : [
          {
            text: 'Ok',
          }
        ],            
      });
      alert.present();
    }

    onMenuItem(m){
      this.router.navigateByUrl(m.url);

    }

    public menus1 = [

      {title1:"Gestion stock", url:"menu/gestionstckok", icon1:'ios-cart'},    
      {title1:"Historique entrées", url:"menu/hist-entrer", icon1:'ios-arrow-round-forward'},    
      {title1:"Historique sorties", url:"menu/hist-sorti", icon1:'ios-arrow-round-back'},    
    ]


    onMenu1Item(m){
      this.router.navigateByUrl(m.url);

    }

    public menus2 = [

      {title2:"Nous Contactez", url:"menu/nouscontacter", icon2:'ios-contacts'},
      {title2:"A Propos", url:"menu/aprospos", icon2:'ios-help-circle-outline'},
      
    ]


    onMenu2Item(m){
      this.router.navigateByUrl(m.url);

    }

    public logo = {
      logo : "assets/images/money.png",

    }

}
