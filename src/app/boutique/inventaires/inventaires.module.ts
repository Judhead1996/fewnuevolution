import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { InventairesPage } from './inventaires.page';

const routes: Routes = [
  {
    path: '',
    component: InventairesPage,
    children: [
      { path: 'journalier-inv', loadChildren: '../../boutique/journalier-inv/journalier-inv.module#JournalierInvPageModule' },

      { path: 'mensuel-inv', loadChildren: '../../boutique/mensuel-inv/mensuel-inv.module#MensuelInvPageModule' },

      {
        path: '',
        redirectTo: 'menu/inventaires/journalier-inv',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: 'menu/inventaires/journalier-inv',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [InventairesPage]
})
export class InventairesPageModule {}
