import { Component, OnInit } from '@angular/core';
import { PublicService } from 'src/app/services/data-api/public.service';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-pret',
  templateUrl: './pret.page.html',
  styleUrls: ['./pret.page.scss'],
})
export class PretPage implements OnInit {

  constructor(private publc:PublicService, 
    private storage: Storage) { }

  keyp: string = 'pret';

  result : any ;

  ngOnInit() {
    this.onLoadPret();
    this.loadData();
  }

  onLoadPret() {
    this.publc.getPret()
    .then(data=>{
     
     this.result = JSON.parse(data.data);
     console.log(this.result.status)  
    });
  }

  doRefresh(event) {
    console.log('Begin async operation');
    this.publc.getPret()
    .then(data=>{
     
     this.result = JSON.parse(data.data);
     console.log(this.result.status) 

    });

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
    this.loadData();
  }

   //////////////////////////////////////
   loadData(){
    this.storage.get(this.keyp).then((val) =>{
      if(val!=null && val != undefined){
        this.result = JSON.parse(val);
      }
    })
  }
  

}
