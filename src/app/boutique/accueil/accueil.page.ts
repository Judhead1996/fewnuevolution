import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.page.html',
  styleUrls: ['./accueil.page.scss'],
})
export class AccueilPage implements OnInit {

  constructor(private alertCtrl: AlertController, public loadingCtrl: LoadingController,
    private router:Router,  private storage: Storage) { }

  ngOnInit() {
  }

  public logo = {
    logo : "assets/images/mc2.png",

  }

  async presentConfirm() {
    let alert = await this.alertCtrl.create({
      header: 'Déconnexion',
      message: 'Voulez vous vraiment vous déconnecter?',
      buttons: [
        {
          text: 'Non',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Oui',
          handler: () => {
            console.log('Buy clicked');
            this.logout();
          }
        }
      ]
    });
    alert.present();
  }

  async logout() {
    var loader = await this.loadingCtrl.create({
      message: "Deconnexion...",
      duration: 2000
    });
    await loader.present();
  
  
      localStorage.removeItem('token'); 
      this.remoVelocale();
      loader.dismiss();
      this.router.navigateByUrl('/beforelogin');
    }

    remoVelocale(){
      this.storage.remove('vente');
      this.storage.remove('depense');
      this.storage.remove('pret');
      this.storage.remove('entree');
      this.storage.remove('sortie');
    }

}
