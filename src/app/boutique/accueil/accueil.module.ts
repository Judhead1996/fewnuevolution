import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AccueilPage } from './accueil.page';

const routes: Routes = [
  {
    path: '',
    component: AccueilPage,
    children: [

      { 
        path: 'vente', 
        loadChildren: '../../boutique/vente/vente.module#VentePageModule'
       },
      
      { 
        path: 'depense',
        loadChildren: '../../boutique/depense/depense.module#DepensePageModule' 
      },

      {
        path: 'pret',
        loadChildren: '../../boutique/pret/pret.module#PretPageModule'
      },

      {
          path: '',
          redirectTo: 'menu/accueil/vente',
          pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: 'menu/accueil/vente',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AccueilPage]
})
export class AccueilPageModule {}
