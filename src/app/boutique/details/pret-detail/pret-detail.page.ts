import { Component, OnInit } from '@angular/core';
import { PublicService } from 'src/app/services/data-api/public.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NavController, LoadingController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-pret-detail',
  templateUrl: './pret-detail.page.html',
  styleUrls: ['./pret-detail.page.scss'],
})
export class PretDetailPage implements OnInit {

  result : any ;

  id = null;
  Client = "";
  Telephone = "";
  Designation = "";
  Montant = "";
  Date = "";

  constructor(public  publcService:PublicService, private router:Router, private activatedRoute: ActivatedRoute,
    public navCtrl: NavController,
    public loadingCtrl: LoadingController, private alertCtrl: AlertController) { }

  ngOnInit() {
    this.onLoadHistoPret();
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.Date = this.activatedRoute.snapshot.paramMap.get('Date');
    this.Client = this.activatedRoute.snapshot.paramMap.get('Client');
    this.Telephone = this.activatedRoute.snapshot.paramMap.get('Telephone');
    this.Designation = this.activatedRoute.snapshot.paramMap.get('Designation');
    this.Montant = this.activatedRoute.snapshot.paramMap.get('Montant');
    console.log(this.Designation)
    localStorage.setItem('idPret', this.id)
  }

  async onModif(pret){ 
    if(pret.prete == "Remboursement"){
      var loader = await this.loadingCtrl.create({
        message: "Modification...",
        duration: 2000
      });
      await loader.present();

      var id = localStorage.getItem('userId');
      this.publcService.updatePret({
      Client: pret.Nom_Client,
      Designation: pret.Libelle,
      Montant:  parseInt(this.Montant) + pret.Montant,
      Telephone: pret.Phone,
      id_userp: id,
      })
      .then(data =>{
        
        if(data){
        
          loader.dismiss();
         this.router.navigateByUrl('/menu/accueil/pret');
         console.log('pret');

       }else{
         this.router.navigateByUrl('modifier-pret');
         console.log('échec') 
       }
     });

      }else{

        var loader = await this.loadingCtrl.create({
          message: "Modification...",
          duration: 2000
        });
        await loader.present();

        var id = localStorage.getItem('userId');
        
        this.publcService.updatePret({
        Client: pret.Nom_Client,
        Designation: pret.Libelle,
        Montant: parseInt(this.Montant) - pret.Montant,
        Telephone: pret.Phone, 
        id_userp: id,
      })
      .then(data =>{ 
        
        if(data){
         loader.dismiss();
         this.router.navigateByUrl('menu/accueil/pret');
         console.log('ajout réussi') 

       }else{
         this.router.navigateByUrl('modifier-pret');
         console.log('échec') 
       }
     });

    }

    /*
      this.publcService.ajoutHistoPret({

        Client: pret.Nom_Client,
        Designation: pret.Libelle,
        Montant: pret.Montant,
        Telephone: pret.Phone,
        id_userp: id,
       })
       */ 

  }

  async suppConfirm(id) {
    let alert = await this.alertCtrl.create({
      header: 'Suppression',
      message: 'Voulez vous vraiment le supprimer?',
      buttons: [
        {
          text: 'Non',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Oui',
          handler: () => {
            console.log('Buy clicked');
            this.deletePret(id);
          }
        }
      ]
    });
    alert.present();
  }

  async deletePret(id){
    var loader = await this.loadingCtrl.create({
      message: "Suppression...",
      duration: 2000
    });
    await loader.present();
    this.publcService.deletePret(id).then(()=>{
      loader.dismiss();
      this.router.navigateByUrl('/menu/accueil/pret');
    })
  }

  //////////////////////////

  

  //////////////////////////
  onLoadHistoPret() {
    this.publcService.getHistoPret()
    .then(data=>{
     
     this.result = JSON.parse(data.data);
     console.log(this.result.status)  
    },err=>{
      console.log(err); 
    });
  }

  doRefresh(event) {
    console.log('Begin async operation');
    this.publcService.getHistoPret()
    .then(data=>{
     
     this.result = JSON.parse(data.data);
     console.log(this.result.status)      
    });

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }


}
