import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PretDetailPage } from './pret-detail.page';

describe('PretDetailPage', () => {
  let component: PretDetailPage;
  let fixture: ComponentFixture<PretDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PretDetailPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PretDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
