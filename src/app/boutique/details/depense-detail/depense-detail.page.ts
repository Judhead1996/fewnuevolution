import { Component, OnInit } from '@angular/core';
import { PublicService } from 'src/app/services/data-api/public.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NavController, LoadingController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-depense-detail',
  templateUrl: './depense-detail.page.html',
  styleUrls: ['./depense-detail.page.scss'],
})
export class DepenseDetailPage implements OnInit {

  id = null;
  Designation = "";
  Prix = "";
  Date = "";

  constructor(public  publcService:PublicService, private router:Router,
     private activatedRoute: ActivatedRoute
     , public navCtrl: NavController,
    public loadingCtrl: LoadingController, private alertCtrl: AlertController) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.Date = this.activatedRoute.snapshot.paramMap.get('Date');
    this.Designation = this.activatedRoute.snapshot.paramMap.get('Designation');
    this.Prix = this.activatedRoute.snapshot.paramMap.get('Prix');
    console.log(this.Designation)
    localStorage.setItem('idDepense', this.id)
  }

  async onModif(depense){ 
    var loader = await this.loadingCtrl.create({
      message: "Modification...",
      duration: 2000
    });
    await loader.present();

    var id = localStorage.getItem('userId'); 
       this.publcService.updateDepense({
         Designation: depense.Designation,
         Prix: depense.Prix,
         id_userv: id,
       })
       .then(data =>{
         console.log(data);
         if(data){
          loader.dismiss();
          this.router.navigateByUrl('/menu/accueil/depense');
          
        }else{
          this.router.navigateByUrl('ajoutdepense');
          console.log('échec') 
        }
      });
      console.log(depense);
  }

  async suppConfirm(id) {
    let alert = await this.alertCtrl.create({
      header: 'Suppression',
      message: 'Voulez vous vraiment le supprimer?',
      buttons: [
        {
          text: 'Non',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Oui',
          handler: () => {
            console.log('Buy clicked');
            this.deleteDepense(id);
          }
        }
      ]
    });
    alert.present();
  }


  async deleteDepense(id){
    var loader = await this.loadingCtrl.create({
      message: "Supression...",
      duration: 2000
    });
    await loader.present();

    this.publcService.deleteDepense(id).then(()=>{
      loader.dismiss();
      this.router.navigateByUrl('/menu/accueil/depense');
    })
  }

}
