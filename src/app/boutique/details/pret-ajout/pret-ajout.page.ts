import { Component, OnInit } from '@angular/core';
import { PublicService } from 'src/app/services/data-api/public.service';
import { LoadingController, NavController } from '@ionic/angular';
import { Router } from '@angular/router';


@Component({
  selector: 'app-pret-ajout',
  templateUrl: './pret-ajout.page.html',
  styleUrls: ['./pret-ajout.page.scss'],
})
export class PretAjoutPage implements OnInit {

  constructor(private route:Router, public  publicService: PublicService, private router:Router, public navCtrl: NavController,
    public loadingCtrl: LoadingController ) { }

  ngOnInit() {
  }

    
  async onAjout(pret) {

    var loader = await this.loadingCtrl.create({
      message: "Chargement...",
      duration: 2000
    });
    await loader.present();

      {
  var id = localStorage.getItem('userId');
  this.publicService.ajoutpret({

   Designation:pret.Libelle,
   Montant: - pret.Montant,
   Client:pret.Nom_Client,
   Telephone: pret.Phone,
   id_userp:id,
  })
  .then(data =>{
    console.log(data);
    loader.dismiss();
   if(data){
    this.router.navigateByUrl('menu/accueil/pret');
     console.log('ajout réussi')
  }else{
    this.router.navigateByUrl('ajoutvente');
     console.log('échec') 
  }
  });
  }
  
  this.publicService.ajoutHistoPret({

    Designation:pret.Libelle,
    Montant: - pret.Montant,
    Client:pret.Nom_Client,
    Telephone: pret.Phone,
    id_userp:id,
   })

 }
 

}
