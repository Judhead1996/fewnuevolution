import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PretAjoutPage } from './pret-ajout.page';

describe('PretAjoutPage', () => {
  let component: PretAjoutPage;
  let fixture: ComponentFixture<PretAjoutPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PretAjoutPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PretAjoutPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
