import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VenteDetailPage } from './vente-detail.page';

describe('VenteDetailPage', () => {
  let component: VenteDetailPage;
  let fixture: ComponentFixture<VenteDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VenteDetailPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VenteDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
