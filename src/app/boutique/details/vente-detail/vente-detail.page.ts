import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoadingController, NavController, AlertController } from '@ionic/angular';
import { PublicService } from 'src/app/services/data-api/public.service';

@Component({
  selector: 'app-vente-detail',
  templateUrl: './vente-detail.page.html',
  styleUrls: ['./vente-detail.page.scss'],
})
export class VenteDetailPage implements OnInit {

  id = null;  
  Designation = "";
  Prix = "";
  Client = "";
  Date = "";

  constructor(public  publcService:PublicService, private router:Router, 
    private activatedRoute: ActivatedRoute,
     public navCtrl: NavController,
    public loadingCtrl: LoadingController, private alertCtrl: AlertController) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.Designation = this.activatedRoute.snapshot.paramMap.get('Designation');
    this.Prix = this.activatedRoute.snapshot.paramMap.get('Prix');
    this.Client = this.activatedRoute.snapshot.paramMap.get('Client');
    this.Date = this.activatedRoute.snapshot.paramMap.get('Date');
    console.log(this.Designation);
    localStorage.setItem('idVente', this.id);  
  }

  async onModif(vente){ 
    var loader = await this.loadingCtrl.create({
      message: "Modification...",
      duration: 2000
    });
    await loader.present();

    var id = localStorage.getItem('userId');  
       this.publcService.updateVente({ 
         Designation: vente.Designation,
         Prix: vente.Prix,
         Client: vente.Client,
         id_userv: id,
       })
       .then(data =>{
         console.log(data);
         if(data){    
          loader.dismiss();
          this.router.navigateByUrl('/menu/accueil/vente');   
          console.log('update réussi')   
        }else{
         
          loader.dismiss();
        
        }
      });
  }

  async suppConfirm(id) {
    let alert = await this.alertCtrl.create({
      header: 'Suppression',
      message: 'Voulez vous vraiment le supprimer?',
      buttons: [
        {
          text: 'Non',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Oui',
          handler: () => {
            console.log('Buy clicked');
            this.deleteVente(id);
          }
        }
      ]
    });
    alert.present();
  }

  async deleteVente(id){
    var loader = await this.loadingCtrl.create({
      message: "Supression...",
      duration: 2000
    });
    await loader.present();

    this.publcService.deleteVente(id).then(()=>{
      // this.VentePage.onLoadVente();
      loader.dismiss();
      this.router.navigateByUrl('/menu/accueil/vente');
    })
  }

}
