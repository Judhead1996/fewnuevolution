import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PublicService } from 'src/app/services/data-api/public.service';


@Component({
  selector: 'app-journalier-inv',
  templateUrl: './journalier-inv.page.html',
  styleUrls: ['./journalier-inv.page.scss'],
})
export class JournalierInvPage implements OnInit {

  formatteDate; 
  
  tab1 = []; tab3 = []; tab4 = []; tab5 = []; tab6 = []; tab7 = []; tab8 = []; tab9 = []; tab10 = []; tab11 = []; tab12 = []; tab13 = []; tab14 = [];tab15 = [];tab16 = [];tab17 = [];tab18 = [];tab19 = [];tab20 = [];tab21 = [];tab22 = [];tab23 = [];tab24 = [];tab25 = [];tab26 = [];tab27 = [];tab28 = [];tab29 = [];tab30 = [];tab31 = [];

  currentMois3: any;
  si4: any; si5: any; si6: any; si7: any; si8: any; si9: any; si10: any; si11: any; si12: any; si13: any; si14: any; si15: any; si16: any; si17: any; si18: any; si19: any; si20: any; si21: any; si22: any; si23: any; si24: any; si25: any; si26: any; si27: any; si28: any; si29: any; si30: any; si31: any;
 
  formatteDate1: any;
  formatteDate2: any;
  formatteDate3: any;
  formatteDate4: any;
  formatteDate5: any;
  formatteDate6: any;
  formatteDate7: any;
  formatteDate8: any;
  formatteDate9: any;
  formatteDate10: any;
  formatteDate11: any;
  formatteDate12: any;
  formatteDate13: any;
  formatteDate14: any;
  formatteDate15: any;
  formatteDate16: any;
  formatteDate17: any;
  formatteDate18: any;
  formatteDate19: any;
  formatteDate20: any;
  formatteDate21: any;
  formatteDate22: any;
  formatteDate23: any;
  formatteDate24: any;
  formatteDate25: any;
  formatteDate26: any;
  formatteDate27: any;
  formatteDate28: any;
  formatteDate29: any;
  formatteDate30: any;
  formatteDate31: any;

  constructor(private publc: PublicService, private router: Router) { }

  myDate = new Date().toISOString();

  debutsem = new Date();

  result: any;
  sommeDay: any;
  

  listToday = [];
  listHier = [];
  listTroisieme = [];
  listQuatrieme = [];
  listCinquieme = [];
  listSixieme = [];
  listSeptieme = [];
  listHuitieme = [];
  listNeuvieme = [];
  listDixieme = [];
  listOnzieme = [];
  listDouzieme = [];
  listTresieme = [];
  listQuatorzieme = [];
  listQuinzieme = [];
  listSezieme = [];
  listDixseptieme = [];
  listDixhuitieme = [];
  listDixneuvieme = [];
  listVingtieme = [];
  listVingtune = [];
  listVingtdeux = [];
  listVingttrois = [];
  listVingtquatre = [];
  listVingtcinq = [];
  listVingtsix = [];
  listVingtsept = [];
  listVingthuit = [];
  listVingtneuve = [];
  listTrante = [];
  listTranteun = [];

  som_1: any;
  som_2: any;
  som_3: any;
  som_4: any;
  som_5: any;
  som_6: any;
  som_7: any;
  som_8: any;
  som_9: any;
  som_10: any;
  som_11: any;
  som_12: any;
  som_13: any;
  som_14: any;
  som_15: any;
  som_16: any;
  som_17: any;
  som_18: any;
  som_19: any;
  som_20: any;
  som_21: any;
  som_22: any;
  som_23: any;
  som_24: any;
  som_25: any;
  som_26: any;
  som_27: any;
  som_28: any;
  som_29: any;
  som_30: any;
  som_31: any;
  

  ngOnInit() {
    this.onLoadVente();
    this.getFormatteDate3()
    this.getFormatteDate4()
    this.getFormatteDate5()
    this.getFormatteDate6()
    this.getFormatteDate7()
    this.getFormatteDate8()
    this.getFormatteDate9()
    this.getFormatteDate10()
    this.getFormatteDate11()
    this.getFormatteDate12()
    this.getFormatteDate13()
    this.getFormatteDate14()
    this.getFormatteDate15()
    this.getFormatteDate16()
    this.getFormatteDate17()
    this.getFormatteDate18()
    this.getFormatteDate19()
    this.getFormatteDate20()
    this.getFormatteDate21()
    this.getFormatteDate22()
    this.getFormatteDate23()
    this.getFormatteDate24()
    this.getFormatteDate25()
    this.getFormatteDate26()
    this.getFormatteDate27()
    this.getFormatteDate28()
    this.getFormatteDate29()
    this.getFormatteDate30()
    this.getFormatteDate31()
  }

  onLoadVente() {
    this.publc.getVente()
      .then(data => {

        this.result = JSON.parse(data.data);
        console.log(this.result.status)
        this.listing();

      });
  }

  doRefresh(event) {
    console.log('Begin async operation');

    this.publc.getVente()
      .then(data => {
        
        this.result = JSON.parse(data.data);
        console.log(this.result.status)

        this.listing();

      });
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }

  dateDay() {
    var tab = this.myDate.split('-');

    let date = {
      annee: tab[0],
      mois: tab[1],
      jour: tab[2].split('T')[0],
      hier: (tab[2], parseInt(tab[2]) - 1),
      troisiemeD: (parseInt(tab[2]) - 2),
      quatriemeD: (parseInt(tab[2]) - 3),
      cinquiemeD: (parseInt(tab[2]) - 4),
      sixiemeD: (parseInt(tab[2]) - 5),
      septiemeD: (parseInt(tab[2]) - 6),
      huitiemeD: (parseInt(tab[2]) - 7),
      neuviemeD: (parseInt(tab[2]) - 8),
      dixiemeD: (parseInt(tab[2]) - 9),
      onziemeD: (parseInt(tab[2]) - 10),
      douzD: (tab[2], parseInt(tab[2]) - 11),
      trezD: (parseInt(tab[2]) - 12),
      quatorzD: (parseInt(tab[2]) - 13),
      quinzD: (parseInt(tab[2]) - 14),
      sezD: (parseInt(tab[2]) - 15),
      dixseptD: (parseInt(tab[2]) - 16),
      dixhuitD: (parseInt(tab[2]) - 17),
      dixneuveD: (parseInt(tab[2]) - 18),
      vingtD: (parseInt(tab[2]) - 19),
      vingtunD: (parseInt(tab[2]) - 20),
      vingtdeuxD: (tab[2], parseInt(tab[2]) - 21),
      vingttroisD: (parseInt(tab[2]) - 22),
      vingtquatD: (parseInt(tab[2]) - 23),
      vingtcinqD: (parseInt(tab[2]) - 24),
      vingtsixD: (parseInt(tab[2]) - 25),
      vingtseptD: (parseInt(tab[2]) - 26),
      vingthuitD: (parseInt(tab[2]) - 27),
      vingtneuvD: (parseInt(tab[2]) - 28),
      tranteD: (parseInt(tab[2]) - 29),
      tranteunD: (parseInt(tab[2]) - 30),
    }

    return date;
  }

  listing() {
    let today = this.dateDay();
    
    this.result.data.forEach(produit => {

      let tab = produit.Date.split('-');
      let date = {
        annee: tab[0],
        mois: tab[1],
        jour: tab[2].split('T')[0],
      }
     

      let som1;
      /////////////// condition pour jour present
      if (today.annee == date.annee && today.jour == date.jour && today.mois == date.mois) {
        // this.listToday.clear(this.listToday)
        // console.log(this.listToday.push(produit));
        som1 += produit.Prix;
        this.som_1 = som1;
        this.formatteDate1 = produit.Date

      }

      let som2;
      /////////////// condition pour the day before
      if (today.annee == date.annee && today.hier == date.jour && today.mois == date.mois) {
        // this.listHier.clear(this.listHier)
        // this.listHier.splice(0, this.listHier.length)
        // console.log("hier", this.listHier.push(produit));
        som2 += produit.Prix;
        this.som_2 = som2;
        this.formatteDate2 = produit.Date
      }

      let som3;
      /////////// jour -3
      if (today.annee == date.annee && today.troisiemeD == date.jour && today.mois == date.mois) {
        // this.listTroisieme.splice(0, this.listTroisieme.length)
        // console.log("jour - 3", this.listTroisieme.push(produit));
        som3 += produit.Prix;
        this.som_3 = som3;
        this.formatteDate3 = produit.Date
      }

      let som4;
      /////////// jour -4
      if (today.annee == date.annee && today.quatriemeD == date.jour && today.mois == date.mois) {
        // this.listQuatrieme.splice(0, this.listQuatrieme.length)
        // console.log("jour -4", this.listQuatrieme.push(produit));
        som4 += produit.Prix;
        this.som_4 = som4;
        this.formatteDate4 = produit.Date
      }

      let som5;
      /////////// jour -5
      if (today.annee == date.annee && today.cinquiemeD == date.jour && today.mois == date.mois) {
       // this.listCinquieme.splice(0, this.listCinquieme.length)
        // console.log("jour -5", this.listCinquieme.push(produit));
        som5 += produit.Prix;
        this.som_5 = som5;
        this.formatteDate5 = produit.Date
      }
      
      let som6;
      /////////// jour -6
      if (today.annee == date.annee && today.sixiemeD == date.jour && today.mois == date.mois) {
        // this.listSixieme.splice(0, this.listSixieme.length)
        // console.log("jour -6", this.listSixieme.push(produit));
        som6 += produit.Prix;
        this.som_6 = som6;
        this.formatteDate6 = produit.Date
      }

      let som7
      /////////// jour -7
      if (today.annee == date.annee && today.septiemeD == date.jour && today.mois == date.mois) {
        // this.listSeptieme.splice(0, this.listSeptieme.length)
        // console.log("jour -7", this.listSeptieme.push(produit));
        som7 += produit.Prix;
        this.som_7 = som7;
        this.formatteDate7 = produit.Date
      } 

      let som8;
      /////////// jour -8
      if (today.annee == date.annee && today.huitiemeD == date.jour && today.mois == date.mois) {
       // this.listHuitieme.splice(0, this.listHuitieme.length)
        // console.log("jour -8", this.listHuitieme.push(produit));
        som8 += produit.Prix;
        this.som_8 = som8;
        this.formatteDate8 = produit.Date
      }

      let som9;
      /////////// jour -9
      if (today.annee == date.annee && today.neuviemeD == date.jour && today.mois == date.mois) {
        // this.listNeuvieme.splice(0, this.listNeuvieme.length)
        // console.log("jour -9", this.listNeuvieme.push(produit));
        som9 += produit.Prix;
        this.som_9 = som9;
        this.formatteDate9 = produit.Date
      }

      let som10;
      /////////// jour -10
      if (today.annee == date.annee && today.dixiemeD == date.jour && today.mois == date.mois) {
        // this.listDixieme.splice(0, this.listDixieme.length)
        // console.log("jour -10", this.listDixieme.push(produit));
        som10 += produit.Prix;
        this.som_10 = som10;
        this.formatteDate10 = produit.Date
      }

      let som11;
       /////////// jour -11
       if (today.annee == date.annee && today.onziemeD == date.jour && today.mois == date.mois) {
        // this.listOnzieme.splice(0, this.listOnzieme.length)
        // console.log("jour -11", this.listOnzieme.push(produit));
        som11 += produit.Prix;
        this.som_11 = som11;
        this.formatteDate11 = produit.Date
      }

      let som12;
       /////////// jour -12
       if (today.annee == date.annee && today.douzD == date.jour && today.mois == date.mois) {
        // this.listDouzieme.splice(0, this.listDouzieme.length)
        // console.log("jour -12", this.listDouzieme.push(produit));
        som12 += produit.Prix;
        this.som_12 = som12;
        this.formatteDate12 = produit.Date
      }

      let som13;
       /////////// jour -13
       if (today.annee == date.annee && today.trezD == date.jour && today.mois == date.mois) {
        // this.listTresieme.splice(0, this.listTresieme.length)
        // console.log("jour -13", this.listTresieme.push(produit));
        som13 += produit.Prix;
        this.som_13 = som13;
        this.formatteDate13 = produit.Date
      }
      
      let som14;
       /////////// jour -14
       if (today.annee == date.annee && today.quatorzD == date.jour && today.mois == date.mois) {
        // this.listQuatorzieme.splice(0, this.listQuatorzieme.length)
        // console.log("jour -14", this.listQuatorzieme.push(produit));
        som14 += produit.Prix;
        this.som_14 = som14;
        this.formatteDate14 = produit.Date
      }

      let som15;
       /////////// jour -15
       if (today.annee == date.annee && today.quinzD == date.jour && today.mois == date.mois) {
        // this.listQuinzieme.splice(0, this.listQuinzieme.length)
        // console.log("jour -15", this.listQuinzieme.push(produit));
        som15 += produit.Prix;
        this.som_15 = som15;
        this.formatteDate15 = produit.Date
      }

      let som16;
       /////////// jour -16
       if (today.annee == date.annee && today.sezD == date.jour && today.mois == date.mois) {
        // this.listSezieme.splice(0, this.listSezieme.length)
        // console.log("jour -16", this.listSezieme.push(produit));
        som16 += produit.Prix;
        this.som_16 = som16;
        this.formatteDate16 = produit.Date
      }

      let som17;
       /////////// jour -17
       if (today.annee == date.annee && today.dixseptD == date.jour && today.mois == date.mois) {
        // this.listDixseptieme.splice(0, this.listDixseptieme.length)
        // console.log("jour -17", this.listDixseptieme.push(produit));
        som17 += produit.Prix;
        this.som_17 = som17;
        this.formatteDate17 = produit.Date
      }

      let som18;
       /////////// jour -18
       if (today.annee == date.annee && today.dixhuitD == date.jour && today.mois == date.mois) {
        // this.listDixhuitieme.splice(0, this.listDixhuitieme.length)
        // console.log("jour -18", this.listDixhuitieme.push(produit));
        som18 += produit.Prix;
        this.som_18 = som18;
        this.formatteDate18 = produit.Date
      }

      let som19;
       /////////// jour -19
       if (today.annee == date.annee && today.dixneuveD == date.jour && today.mois == date.mois) {
        // this.listDixneuvieme.splice(0, this.listDixneuvieme.length)
        // console.log("jour -19", this.listDixneuvieme.push(produit));
        som19 += produit.Prix;
        this.som_19 = som19;
        this.formatteDate19 = produit.Date
      }

      let som20;
       /////////// jour -20
       if (today.annee == date.annee && today.vingtD == date.jour && today.mois == date.mois) {
       // this.listVingtieme.splice(0, this.listVingtieme.length)
        // console.log("jour -20", this.listVingtieme.push(produit));
        som20 += produit.Prix;
        this.som_20 = som20;
        this.formatteDate20 = produit.Date
      }

      let som21;
       /////////// jour -21
       if (today.annee == date.annee && today.vingtunD == date.jour && today.mois == date.mois) {
        // this.listVingtune.splice(0, this.listVingtune.length)
        // console.log("jour -21", this.listVingtune.push(produit));
        som21 += produit.Prix;
        this.som_21 = som21;
        this.formatteDate21 = produit.Date
      }

      let som22;
       /////////// jour -22
       if (today.annee == date.annee && today.vingtdeuxD == date.jour && today.mois == date.mois) {
        // this.listVingtdeux.splice(0, this.listVingtdeux.length)
        // console.log("jour -22", this.listVingtdeux.push(produit));
        som22 += produit.Prix;
        this.som_22 = som22;
        this.formatteDate22 = produit.Date
      }

      let som23;
       /////////// jour -23
       if (today.annee == date.annee && today.vingttroisD == date.jour && today.mois == date.mois) {
        // this.listVingttrois.splice(0, this.listVingttrois.length)
        // console.log("jour -23", this.listVingttrois.push(produit));
        som23 += produit.Prix;
        this.som_23 = som23;
        this.formatteDate23 = produit.Date
      }

      let som24;
       /////////// jour -24
       if (today.annee == date.annee && today.vingtquatD == date.jour && today.mois == date.mois) {
       // this.listVingtquatre.splice(0, this.listVingtquatre.length)
        // console.log("jour -24", this.listVingtquatre.push(produit));
        som24 += produit.Prix;
        this.som_24 = som24;
        this.formatteDate24 = produit.Date
      }

      let som25;
       /////////// jour -25
       if (today.annee == date.annee && today.vingtcinqD == date.jour && today.mois == date.mois) {
        // this.listVingtcinq.splice(0, this.listVingtcinq.length)
        // console.log("jour -25", this.listVingtcinq.push(produit));
        som25 += produit.Prix;
        this.som_25 = som25;
        this.formatteDate25 = produit.Date
      }

      let som26;
       /////////// jour -26
       if (today.annee == date.annee && today.vingtsixD == date.jour && today.mois == date.mois) {
        // this.listVingtsix.splice(0, this.listVingtsix.length)
        // console.log("jour -26", this.listVingtsix.push(produit));
        som26 += produit.Prix;
        this.som_26 = som26;
        this.formatteDate26 = produit.Date
      }

      let som27;
       /////////// jour -27
       if (today.annee == date.annee && today.vingtseptD == date.jour && today.mois == date.mois) {
        // this.listVingtsept.splice(0, this.listVingtsept.length)
        // console.log("jour -27", this.listVingtsept.push(produit));
        som27 += produit.Prix;
        this.som_27 = som27;
        this.formatteDate27 = produit.Date
      }

      let som28;
       /////////// jour -28
       if (today.annee == date.annee && today.vingthuitD == date.jour && today.mois == date.mois) {
        // this.listVingthuit.splice(0, this.listVingthuit.length)
        // console.log("jour -28", this.listVingthuit.push(produit));
        som28 += produit.Prix;
        this.som_28 = som28;
        this.formatteDate28 = produit.Date
      }

      let som29;
       /////////// jour -29
       if (today.annee == date.annee && today.vingtneuvD == date.jour && today.mois == date.mois) {
        // this.listVingtneuve.splice(0, this.listVingtneuve.length)
        // console.log("jour -29", this.listVingtneuve.push(produit));
        som29 += produit.Prix;
        this.som_29 = som29;
        this.formatteDate29 = produit.Date
      }

      let som30;
       /////////// jour -30
       if (today.annee == date.annee && today.tranteD == date.jour && today.mois == date.mois) {
       // this.listTrante.splice(0, this.listTrante.length)
        // console.log("jour -30", this.listTrante.push(produit));
        som30 += produit.Prix;
        this.som_30 = som30;
        this.formatteDate30 = produit.Date
      }

      let som31;
       /////////// jour -31
       if (today.annee == date.annee && today.tranteunD == date.jour && today.mois == date.mois) {
       // this.listTranteun.splice(0, this.listTranteun.length)
        // console.log("jour -31", this.listTranteun.push(produit));
        som31 += produit.Prix;
        this.som_31 = som31;
        this.formatteDate31 = produit.Date
      }
    

    });
  }

 

  deleteVente(id) {
    this.publc.deleteVente(id).then(() => {
      this.onLoadVente();
    })
  }

  updateVente(id) {
    this.publc.updateVente(id).then(() => {
      this.onLoadVente();
    })
  }

  getFormatteDate3() {
    let dateobj3 = new Date();
    dateobj3.setDate(dateobj3.getDate() - 2);
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    this.formatteDate = jours[dateobj3.getDay()] + " " + dateobj3.getDate() + " " + mois[dateobj3.getMonth()];
    this.tab3 = this.formatteDate;

    this.currentMois3 = mois[dateobj3.getMonth()];
  }
    
  getFormatteDate4() {
    var dateobj4 = new Date()
    dateobj4.setDate(dateobj4.getDate() - 3);
    var jours = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    this.formatteDate = jours[dateobj4.getDay()] + " " + dateobj4.getDate() + " " + mois[dateobj4.getMonth()];
    this.tab4 = this.formatteDate;

    this.si4 = mois[dateobj4.getMonth()];
  }

  getFormatteDate5() {
    var dateobj5 = new Date()
    dateobj5.setDate(dateobj5.getDate() - 4);
    this.formatteDate = dateobj5
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    this.formatteDate = jours[dateobj5.getDay()] + " " + dateobj5.getDate() + " " + mois[dateobj5.getMonth()];
    this.tab5 = this.formatteDate;

    this.si5 = mois[dateobj5.getMonth()];
  }

  getFormatteDate6() {
    var dateobj6 = new Date()
    dateobj6.setDate(dateobj6.getDate() - 5);
    this.formatteDate = dateobj6
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    this.formatteDate = jours[dateobj6.getDay()] + " " + dateobj6.getDate() + " " + mois[dateobj6.getMonth()];
    this.tab6 = this.formatteDate;

    this.si6 = mois[dateobj6.getMonth()];
  }

  getFormatteDate7() {
    var dateobj7 = new Date()
    dateobj7.setDate(dateobj7.getDate() - 6);
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    this.formatteDate = jours[dateobj7.getDay()] + " " + dateobj7.getDate() + " " + mois[dateobj7.getMonth()];
    console.log(this.formatteDate)
    this.tab7 = this.formatteDate;

    this.si7 = mois[dateobj7.getMonth()];
  }

  getFormatteDate8() {
    var dateobj8 = new Date()
    dateobj8.setDate(dateobj8.getDate() - 7);
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    this.formatteDate = jours[dateobj8.getDay()] + " " + dateobj8.getDate() + " " + mois[dateobj8.getMonth()];
    this.tab8 = this.formatteDate;

    this.si8 = mois[dateobj8.getMonth()];
  }

  getFormatteDate9() {
    var dateobj9 = new Date()
    dateobj9.setDate(dateobj9.getDate() - 8);    
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    this.formatteDate = jours[dateobj9.getDay()] + " " + dateobj9.getDate() + " " + mois[dateobj9.getMonth()];
    this.tab9 = this.formatteDate;

    this.si9 = mois[dateobj9.getMonth()];
  }

  getFormatteDate10() {
    var dateobj10 = new Date()
    dateobj10.setDate(dateobj10.getDate() - 9);
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    this.formatteDate = jours[dateobj10.getDay()] + " " + dateobj10.getDate() + " " + mois[dateobj10.getMonth()];
    this.tab10 = this.formatteDate;

    this.si10 = mois[dateobj10.getMonth()];
  }

  getFormatteDate11() {
    var dateobj11 = new Date()
    dateobj11.setDate(dateobj11.getDate() - 10);
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    this.formatteDate = jours[dateobj11.getDay()] + " " + dateobj11.getDate() + " " + mois[dateobj11.getMonth()];
    this.tab11 = this.formatteDate;

    this.si11 = mois[dateobj11.getMonth()];
  }

  getFormatteDate12() {
    var dateobj12 = new Date()
    dateobj12.setDate(dateobj12.getDate() - 11);
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    this.formatteDate = jours[dateobj12.getDay()] + " " + dateobj12.getDate() + " " + mois[dateobj12.getMonth()];
    console.log(this.formatteDate)
    this.tab12 = this.formatteDate;

    this.si12 = mois[dateobj12.getMonth()];
  }

  getFormatteDate13() {
    var dateobj13 = new Date()
    dateobj13.setDate(dateobj13.getDate() - 12);
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    this.formatteDate = jours[dateobj13.getDay()] + " " + dateobj13.getDate() + " " + mois[dateobj13.getMonth()];
    this.tab13 = this.formatteDate;

    this.si13 = mois[dateobj13.getMonth()];
  }

  getFormatteDate14() {
    var dateobj14 = new Date()
    dateobj14.setDate(dateobj14.getDate() - 13);
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    this.formatteDate = jours[dateobj14.getDay()] + " " + dateobj14.getDate() + " " + mois[dateobj14.getMonth()];
    this.tab14 = this.formatteDate;

    this.si14 = mois[dateobj14.getMonth()];
  }

  getFormatteDate15() {
    var dateobj15 = new Date()
    dateobj15.setDate(dateobj15.getDate() - 14);
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    this.formatteDate = jours[dateobj15.getDay()] + " " + dateobj15.getDate() + " " + mois[dateobj15.getMonth()];
    this.tab15 = this.formatteDate;

    this.si15 = mois[dateobj15.getMonth()];
  }

  getFormatteDate16() {
    var dateobj16 = new Date()
    dateobj16.setDate(dateobj16.getDate() - 15);
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    this.formatteDate = jours[dateobj16.getDay()] + " " + dateobj16.getDate() + " " + mois[dateobj16.getMonth()];
    this.tab16 = this.formatteDate;

    this.si16 = mois[dateobj16.getMonth()];
  }

  getFormatteDate17() {
    var dateobj17 = new Date()
    dateobj17.setDate(dateobj17.getDate() - 16);
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    this.formatteDate = jours[dateobj17.getDay()] + " " + dateobj17.getDate() + " " + mois[dateobj17.getMonth()];
    this.tab17 = this.formatteDate;

    this.si17 = mois[dateobj17.getMonth()];
  }

  getFormatteDate18() {
    var dateobj18 = new Date()
    dateobj18.setDate(dateobj18.getDate() - 17);
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    this.formatteDate = jours[dateobj18.getDay()] + " " + dateobj18.getDate() + " " + mois[dateobj18.getMonth()];
    this.tab18 = this.formatteDate;

    this.si18 = mois[dateobj18.getMonth()];
  }

  getFormatteDate19() {
    var dateobj19 = new Date()
    dateobj19.setDate(dateobj19.getDate() - 18);
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    this.formatteDate = jours[dateobj19.getDay()] + " " + dateobj19.getDate() + " " + mois[dateobj19.getMonth()];
    this.tab19 = this.formatteDate;

    this.si19 = mois[dateobj19.getMonth()];
  }

  getFormatteDate20() {
    var dateobj20 = new Date()
    dateobj20.setDate(dateobj20.getDate() - 19);
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    this.formatteDate = jours[dateobj20.getDay()] + " " + dateobj20.getDate() + " " + mois[dateobj20.getMonth()];
    this.tab20 = this.formatteDate;

    this.si20 = mois[dateobj20.getMonth()];
  }

  getFormatteDate21() {
    var dateobj21 = new Date()
    dateobj21.setDate(dateobj21.getDate() - 20);
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    this.formatteDate = jours[dateobj21.getDay()] + " " + dateobj21.getDate() + " " + mois[dateobj21.getMonth()];
    this.tab21 = this.formatteDate;

    this.si21 = mois[dateobj21.getMonth()];
  }

  getFormatteDate22() {
    var dateobj22 = new Date()
    dateobj22.setDate(dateobj22.getDate() - 21);
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    this.formatteDate = jours[dateobj22.getDay()] + " " + dateobj22.getDate() + " " + mois[dateobj22.getMonth()];
    this.tab22 = this.formatteDate;

    this.si22 = mois[dateobj22.getMonth()];
  }

  getFormatteDate23() {
    var dateobj23 = new Date()
    dateobj23.setDate(dateobj23.getDate() - 22);
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    this.formatteDate = jours[dateobj23.getDay()] + " " + dateobj23.getDate() + " " + mois[dateobj23.getMonth()];
    this.tab23 = this.formatteDate;

    this.si23 = mois[dateobj23.getMonth()];
  }

  getFormatteDate24() {
    var dateobj24 = new Date()
    dateobj24.setDate(dateobj24.getDate() - 23);
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    this.formatteDate = jours[dateobj24.getDay()] + " " + dateobj24.getDate() + " " + mois[dateobj24.getMonth()];
    this.tab24 = this.formatteDate;

    this.si24 = mois[dateobj24.getMonth()];
  }

  getFormatteDate25() {
    var dateobj25 = new Date()
    dateobj25.setDate(dateobj25.getDate() - 24);
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    this.formatteDate = jours[dateobj25.getDay()] + " " + dateobj25.getDate() + " " + mois[dateobj25.getMonth()];
    this.tab25 = this.formatteDate;

    this.si25 = mois[dateobj25.getMonth()];
  }

  getFormatteDate26() {
    var dateobj26 = new Date()
    dateobj26.setDate(dateobj26.getDate() - 25);
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    this.formatteDate = jours[dateobj26.getDay()] + " " + dateobj26.getDate() + " " + mois[dateobj26.getMonth()];
    this.tab26 = this.formatteDate;

    this.si26 = mois[dateobj26.getMonth()];
  }

  getFormatteDate27() {
    var dateobj27 = new Date()
    dateobj27.setDate(dateobj27.getDate() - 26);
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    this.formatteDate = jours[dateobj27.getDay()] + " " + dateobj27.getDate() + " " + mois[dateobj27.getMonth()];
    this.tab27 = this.formatteDate;

    this.si27 = mois[dateobj27.getMonth()];
  }

  getFormatteDate28() {
    var dateobj28 = new Date()
    dateobj28.setDate(dateobj28.getDate() - 27);
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    this.formatteDate = jours[dateobj28.getDay()] + " " + dateobj28.getDate() + " " + mois[dateobj28.getMonth()];
    this.tab28 = this.formatteDate;

    this.si28 = mois[dateobj28.getMonth()];
  }

  getFormatteDate29() {
    var dateobj29 = new Date()
    dateobj29.setDate(dateobj29.getDate() - 28);
    console.log(dateobj29);
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    this.formatteDate = jours[dateobj29.getDay()] + " " + dateobj29.getDate() + " " + mois[dateobj29.getMonth()];
    console.log(this.formatteDate)
    this.tab29 = this.formatteDate;

    this.si29 = mois[dateobj29.getMonth()];
  }

  getFormatteDate30() {
    var dateobj30 = new Date()
    dateobj30.setDate(dateobj30.getDate() - 29);
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    this.formatteDate = jours[dateobj30.getDay()] + " " + dateobj30.getDate() + " " + mois[dateobj30.getMonth()];
    this.tab30 = this.formatteDate;

    this.si30 = mois[dateobj30.getMonth()];
  }

  getFormatteDate31() {
    var dateobj31 = new Date()
    dateobj31.setDate(dateobj31.getDate() - 30);
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'Aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    this.formatteDate = jours[dateobj31.getDay()] + " " + dateobj31.getDate() + " " + mois[dateobj31.getMonth()];
    this.tab31 = this.formatteDate;

   this.si31 = mois[dateobj31.getMonth()];
  }


}
