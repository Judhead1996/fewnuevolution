import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JournalierInvPage } from './journalier-inv.page';

describe('JournalierInvPage', () => {
  let component: JournalierInvPage;
  let fixture: ComponentFixture<JournalierInvPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JournalierInvPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JournalierInvPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
