import { Component, OnInit } from '@angular/core';
import { PublicService } from 'src/app/services/data-api/public.service';
import { Router } from '@angular/router';
import { LoadingController, NavController, AlertController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-vente',
  templateUrl: './vente.page.html',
  styleUrls: ['./vente.page.scss'],
})
export class VentePage implements OnInit {

  myDate = new Date().toISOString();
  debutsem = new Date();

  constructor(private publc: PublicService, private router: Router,
     public loadingCtrl: LoadingController, private alertCtrl: AlertController,
     private toastController: ToastController,
     private storage: Storage) { }

  key: string = 'vente';

  testData: any;

  result: any;
  somming: any;
  date: any;
  sommeDay: any;
  sommeMoi: any;
  sommSemaine: any;

  ngOnInit() {

    this.getVen();
    this.loadData();
  }

    
  async getVen(){
    var loader = await this.loadingCtrl.create({
      message: "Please Wait...",
      duration: 2000
    });
    await loader.present();

    this.publc.getVente()
    .then(data => {
      loader.dismiss();
      this.result = JSON.parse(data.data);
     
        
      // localStorage.setItem('dataVente', this.result)
      // this.testData = localStorage.getItem('dataVente');
      
      // console.log(this.result.data[0].Prix);

      
      var returnedVal = this.sum([1, 2, 3, 4, 5]);
      console.log(returnedVal);

      this.somming = this.som();
      console.log(this.somming);

      //console.log(this.result.data[0].Date)

      this.sommeToday();

      this.sommeMois();

      this.sommeSemaine();
      
    })

  }

  doRefresh(event) {
    console.log('Begin async operation');

    this.publc.getVente()
      .then(data => {

        this.result = JSON.parse(data.data);
        


        // localStorage.setItem('dataVente', this.result)
        // this.testData = localStorage.getItem('dataVente');
        
        // console.log(this.result.data[0].Prix);

        /**
          var returnedVal = this.sum([1, 2, 3, 4, 5]);
        console.log(returnedVal);

        this.somming = this.som();
        console.log(this.somming);

        //console.log(this.result.data[0].Date)

        this.sommeToday();

        this.sommeMois();

        this.sommeSemaine();
         */
        this.somming = this.som();
        console.log(this.somming);
  
        //console.log(this.result.data[0].Date)
  
        this.sommeToday();
  
        this.sommeMois();
  
        this.sommeSemaine();
       

      });
      
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
    
  }

  ////////

  loadData(){
    this.storage.get(this.key).then((val) =>{
      if(val!=null && val != undefined){
        this.result = JSON.parse(val);
        
        this.somming = this.som();
        console.log(this.somming);
  
        //console.log(this.result.data[0].Date)
  
        this.sommeToday();
  
        this.sommeMois();
  
        this.sommeSemaine();
      }
    })
  }

  dateDay() {
    var tab = this.myDate.split('-');

    let date = {
      annee: tab[0],
      mois: tab[1],
      jour: tab[2].split('T')[0],
    }

    return date;
  }

  som() {
    let s = 0;
    for (let i = 0; i < this.result.data.length; i++) {
      s = s + this.result.data[i].Prix;

    }
    return s;
  }

  sommeToday() {
    let today = this.dateDay();
    let somm = 0;

    this.result.data.forEach(produit => {

      let tab = produit.Date.split('-');
      let date = {
        annee: tab[0],
        mois: tab[1],
        jour: tab[2].split('T')[0],
      }

      if (today.annee == date.annee && today.jour == date.jour && today.mois == date.mois) {

        somm = somm + produit.Prix;
        this.sommeDay = somm;
        localStorage.setItem('totalVenteDay', this.sommeDay)
      }
    });

    console.log("la valeur totale vente du jour ", this.sommeDay)
  }

  sommeMois() {
    let today = this.dateDay();
    let somm = 0;

    this.result.data.forEach(produit => {

      let tab = produit.Date.split('-');
      let date = {
        annee: tab[0],
        mois: tab[1],
        jour: tab[2].split('T')[0],
      }

      if (today.annee == date.annee && today.mois == date.mois) {

        somm = somm + produit.Prix;
        this.sommeMoi = somm;
        localStorage.setItem('totalVenteMois', this.sommeMoi);
      }
    });

    console.log("la valeur totale vente du mois ", this.sommeMoi)
  }

  semaine() {
    let debutsemm = this.debutsem.setUTCDate(this.debutsem.getUTCDate() - this.debutsem.getUTCDay() + 1)
    // console.log(new Date(debutsemm))

    var tab = new Date(debutsemm).toISOString();
    // console.log(tab);
    let tab2 = tab.split('-')
    let hebdo = {                           
      annee: tab2[0],        
      mois: tab2[1],
      lundiSemaine: tab2[2].split('T')[0],
      mardiSemaine: (parseInt(tab2[2].split('T')[0]) + 1),
      mercrediSemaine: (parseInt(tab2[2].split('T')[0]) + 2),
      jeudiSemaine: (parseInt(tab2[2].split('T')[0]) + 3),
      vendrediSemaine: (parseInt(tab2[2].split('T')[0]) + 4),
      samediSemaine: (parseInt(tab2[2].split('T')[0]) + 5),
      dimancheSemaine: (parseInt(tab2[2].split('T')[0]) + 6),
    }
    return hebdo;
  }

  sommeSemaine(){
    let semaineDay = this.semaine();
    let somm = 0;

    this.result.data.forEach(produit => {

      let tab = produit.Date.split('-');
      let date = {
        annee: tab[0],
        mois: tab[1],
        jour: tab[2].split('T')[0],
      }

      if (semaineDay.annee == date.annee && semaineDay.mois == date.mois && semaineDay.lundiSemaine == date.jour || semaineDay.mardiSemaine == date.jour || semaineDay.mercrediSemaine == date.jour || semaineDay.jeudiSemaine == date.jour || semaineDay.vendrediSemaine == date.jour || semaineDay.samediSemaine == date.jour || semaineDay.dimancheSemaine == date.jour) {

        somm = somm + produit.Prix;
        this.sommSemaine = somm;
        localStorage.setItem('totalVenteSemaine', this.sommSemaine);
      }
    });

    console.log("la valeur totale vente de la semaine ", this.sommSemaine)
  }


  sum(arr) {
    let s = 0;
    for (let i = 0; i < 5; i++) {
      s = s + arr[i];
    }
    return s;
  }




  /*
    doRefresh(event) {
      console.log('Begin async operation');
  
      this.publc.getVente()
      .then(data=>{
       
       this.result = JSON.parse(data.data);
       console.log(this.result.count);
      ////////////
      //let s = 0;
      //for (let i=0; i < this.result.data.length; i++) {
       // s = s + this.result.data.Prix[i];
       //console.log(this.result.data.Prix)
       //console.log(i)
      // }
      ///////////
         function sum(arr){
          let s = 0; 
          for (let i=0; i < 5; i++) {
           s = s + arr[i];
           }
           return s;
         }
         var returnedVal = sum([1,2,3,4,5]);
         console.log(returnedVal);
         ////////////
        event.target.complete();
      },err=>{
        console.log(err); 
      });
    }
  */

  deleteVente(id) {
    this.publc.deleteVente(id).then(() => {
      // this.onLoadVente();
    })
  }

  updateVente(id) {
    this.publc.updateVente(id).then(() => {
      // this.onLoadVente();
    })
  }

  async ajoutPrompt() {
    let alert = await this.alertCtrl.create({
      header: 'Produit',
      inputs: [
        {
          name: 'Designation',
          placeholder: 'Désignation'
        },
        {
          name: 'Prix',
          placeholder: 'Prix'
        },
        {
          name: 'Client',
          placeholder: 'Client'
        }, 
      ],
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Ok',
          handler: data => {
           
            if (data.Designation, data.Prix) {

              
                let id = localStorage.getItem('userId');

                this.publc.ajoutvente({
                  Designation: data.Designation,
                  Prix: data.Prix,
                  Client: data.Client,
                  id_userv: id,
                }).then(data => {
      
                this.showToast('réussi');
                this.getVen();
              });
            } else {
              
              return false;
            } 
          }
        }
      ]
    });
    alert.present();
  }

   // Helper
   async showToast(msg){
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

}
