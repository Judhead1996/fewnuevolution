import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JournalierPage } from './journalier.page';

describe('JournalierPage', () => {
  let component: JournalierPage;
  let fixture: ComponentFixture<JournalierPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JournalierPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JournalierPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
