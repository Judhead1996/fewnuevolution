import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-journalier',
  templateUrl: './journalier.page.html',
  styleUrls: ['./journalier.page.scss'],
})
export class JournalierPage implements OnInit {

  myDate = new Date().toISOString();

  totalv = '';
  totald = ''; 
  date= "";

  public imz = {
    logo: "assets/images/journal.jpg",
    logo1: "assets/images/journal1.jpg",
    gif: "assets/images/giphy.gif"
  }

  constructor() { }

  ngOnInit() {
   this.doRefresh(event);
   
  }


  doRefresh(event) {
    console.log('Begin async operation');
    var tab = this.myDate.split('-');
    console.log("tab", tab)
    let date = {
      annee : tab[0],
      mois : tab[1],
      jour : tab[2].split('T')[0],
    }
     
    console.log("Date", date)
    this.dateD();
    this.totalv = localStorage.getItem('totalVenteDay');
    console.log(this.totalv);
    
    this.totald = localStorage.getItem('totalDepenseDay');;
    console.log(this.totald);
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }

  dateChanged(date){
    console.log(date.detail.value)
    console.log(this.myDate)
  }

  dateD(){
    var dateobj3 = new Date()
    dateobj3.setDate(dateobj3.getDate());
    console.log("viens voir",dateobj3.toISOString());
    var jours = ['Dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'];
    var mois = ['janver', 'fevrier', 'mars', 'Avril', 'Mai', 'Juin', 'juillier', 'aout', 'septembre', 'octobre', 'novembre', 'decembre'];
    var formatteDate = jours[dateobj3.getDay()] + " " + dateobj3.getDate() + " " + mois[dateobj3.getMonth()];
    console.log("viens voir 2",formatteDate)
    this.date = formatteDate;
  }

}
