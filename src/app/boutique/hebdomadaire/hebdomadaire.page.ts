import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hebdomadaire',
  templateUrl: './hebdomadaire.page.html',
  styleUrls: ['./hebdomadaire.page.scss'],
})
export class HebdomadairePage implements OnInit {

  totalvv = '';
  totaldd = ''; 

  constructor() { }

  ngOnInit() {
    this.doRefresh(event);
  }

  doRefresh(event){
    this.totalvv = localStorage.getItem('totalVenteSemaine');
    this.totaldd = localStorage.getItem('totalDepenseSemaine');
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }

}
