import { Component, OnInit } from '@angular/core';
import { PublicService } from 'src/app/services/data-api/public.service';
import { Router } from '@angular/router';
import { LoadingController, NavController, AlertController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-depense',
  templateUrl: './depense.page.html',
  styleUrls: ['./depense.page.scss'],
})
export class DepensePage implements OnInit {

  myDate = new Date().toISOString();
  debutsem = new Date();

  constructor(private publc:PublicService, private alertCtrl: AlertController,
    private toastController: ToastController,
    private storage: Storage) { }

  keyd: string = 'depense';

  result : any ;

  somming : any;
  date: any;
  sommeDay: any;
  sommeMoi: any;
  sommSemaine: any;

  ngOnInit() {
    this.doRefresh(event);
    this.loadData()
  }

 // onLoadDepense() {
  //// 
 //// }

  doRefresh(event) {
    console.log('Begin async operation');

    this.publc.getDepense()
    .then(data=>{
     
     this.result = JSON.parse(data.data);
     console.log(this.result.status) 
     
     this.somming = this.som();
     console.log(this.somming);
     
     this.sommeToday();
     
     this.sommeMois();

     this.sommeSemaine()
      
    });

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
   this.loadData();
  }
 
  ////////

  loadData(){
    this.storage.get(this.keyd).then((val) =>{
      if(val!=null && val != undefined){
        this.result = JSON.parse(val);

        this.somming = this.som();
        console.log(this.somming);
        
        this.sommeToday();
        
        this.sommeMois();
   
        this.sommeSemaine()
      }
    })
  }

  dateDay(){
    var tab = this.myDate.split('-');
    
    let date = {
      annee : tab[0],
      mois : tab[1],
      jour : tab[2].split('T')[0],
    }
     
    return date;
  }

  som(){
    let s = 0;
    for (let i=0; i < this.result.data.length; i++) {
      s = s + this.result.data[i].Prix;
     //console.log("nombre de prix",this.result.data[i].Prix) 
    }
    return s;
   }

   sommeToday(){
    let today = this.dateDay();
    let somm = 0;

   this.result.data.forEach(produit => {
     
     let tab = produit.Date.split('-');
     let date = {
       annee : tab[0],
       mois : tab[1],
       jour : tab[2].split('T')[0],
     }
     
     if(today.annee == date.annee && today.jour == date.jour && today.mois == date.mois){
       
       somm = somm + produit.Prix;
       this.sommeDay = somm;
       localStorage.setItem('totalDepenseDay', this.sommeDay)
     }
   });

   console.log("la valeur totale du jour ", this.sommeDay)
 }

 sommeMois(){
  let today = this.dateDay();
  let somm = 0;

 this.result.data.forEach(produit => {
   
   let tab = produit.Date.split('-');
   let date = {
     annee : tab[0],
     mois : tab[1],
     jour : tab[2].split('T')[0],
   }
   
   if(today.annee == date.annee && today.mois == date.mois){
     
     somm = somm + produit.Prix;
     this.sommeMoi = somm;
     localStorage.setItem('totalDepenseMois', this.sommeMoi);
   }
 });

 console.log("la valeur totale depense du mois ", this.sommeMoi)
}

semaine() {
  let debutsemm = this.debutsem.setUTCDate(this.debutsem.getUTCDate() - this.debutsem.getUTCDay() + 1)
  // console.log(new Date(debutsemm))

  var tab = new Date(debutsemm).toISOString();
  // console.log(tab);
  let tab2 = tab.split('-')
  let hebdo = {
    annee: tab2[0],
    mois: tab2[1],
    lundiSemaine: tab2[2].split('T')[0],
    mardiSemaine: (parseInt(tab2[2].split('T')[0]) + 1),
    mercrediSemaine: (parseInt(tab2[2].split('T')[0]) + 2),
    jeudiSemaine: (parseInt(tab2[2].split('T')[0]) + 3),
    vendrediSemaine: (parseInt(tab2[2].split('T')[0]) + 4),
    samediSemaine: (parseInt(tab2[2].split('T')[0]) + 5),
    dimancheSemaine: (parseInt(tab2[2].split('T')[0]) + 6),
  }
  return hebdo;
}

sommeSemaine(){
  let semaineDay = this.semaine();
  let somm = 0;

  this.result.data.forEach(produit => {

    let tab = produit.Date.split('-');
    let date = {
      annee: tab[0],
      mois: tab[1],
      jour: tab[2].split('T')[0],
    }

    if (semaineDay.annee == date.annee && semaineDay.mois == date.mois && semaineDay.lundiSemaine == date.jour || semaineDay.mardiSemaine == date.jour || semaineDay.mercrediSemaine == date.jour || semaineDay.jeudiSemaine == date.jour || semaineDay.vendrediSemaine == date.jour || semaineDay.samediSemaine == date.jour || semaineDay.dimancheSemaine == date.jour) {

      somm = somm + produit.Prix;
      this.sommSemaine = somm;
      localStorage.setItem('totalDepenseSemaine', this.sommSemaine);
    }
  });

  console.log("la valeur totale dépense de la semaine ", this.sommSemaine)
}

async ajoutPrompt() {
  let alert = await this.alertCtrl.create({
    header: 'Produit',
    inputs: [
      {
        name: 'Designation',
        placeholder: 'Désignation'
      },
      {
        name: 'Prix',
        placeholder: 'Prix'
      },
    ],
    buttons: [
      {
        text: 'Annuler',
        role: 'cancel',
        handler: data => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Ok',
        handler: data => {
         
          if (data.Designation, data.Prix) {
              let id = localStorage.getItem('userId');

              this.publc.ajoutdepense({
                Designation: data.Designation,
                Prix: data.Prix,
                id_userd: id,
              }).then(data => {
    
              this.showToast('réussi');
              this.doRefresh(event);
            });
          } else {
            
            return false;
          } 
        }
      }
    ]
  });
  alert.present();
}

 // Helper
 async showToast(msg){
  const toast = await this.toastController.create({
    message: msg,
    duration: 2000
  });
  toast.present();
}

}
