import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MensuelInvPage } from './mensuel-inv.page';

describe('MensuelInvPage', () => {
  let component: MensuelInvPage;
  let fixture: ComponentFixture<MensuelInvPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MensuelInvPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MensuelInvPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
