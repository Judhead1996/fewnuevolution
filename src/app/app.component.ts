import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { StorageService } from './services/storage/storage.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router : Router,
    private storg: StorageService
  ) {
   this.initializeApp();
   this.home();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  private home() {
    if(localStorage.getItem("token")) {
      this.router.navigateByUrl('/home');
    }else{
      this.router.navigateByUrl('/beforelogin');
    }
  }
  
 /*
  getVenteJson(){
    this.storg.getDataVente();
  }

  getDepenseJson(){
    this.storg.getDataDepense();
  }

  getPretJson(){
    this.storg.getDataPret();
  }

  getEntreeJson(){
    this.storg.getDataEntree();
  }

  getSortieJson(){
    this.storg.getDataSortie();
  }
  */
}
