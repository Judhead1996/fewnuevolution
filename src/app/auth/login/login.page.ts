import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  result : any ;
  isLoginError : boolean = false;
  user: boolean = false;
  telephone : any;

  pasInternet = false;

  constructor(private authService: AuthService, private router:Router, private alertController:AlertController) { }

  ngOnInit() {
   /* if(localStorage.getItem("token")) {
      this.user = true;
      this.router.navigateByUrl('/home');
    }*/
  }

 onLogin(value) { 
    this.authService.login({
      phone:value.phone,
      password:value.password,
    
    }).then(data  =>{
      this.result = JSON.parse(data.data);
      console.log('Bienvenue', this.result.nom_complet);
      // recuperation de l'id
      let phone = this.result.phone;
      parseInt(phone)
      localStorage.setItem('userId', phone )
      localStorage.setItem('token', this.result.token.token)
      localStorage.setItem('nom', this.result.nom_complet)
      this.user = true;
      
      if(data){
        this.router.navigateByUrl('/home');
        console.log('success routing')
      }else{
        this.router.navigateByUrl('login');
        console.log(value.password, 'login incorrect') 
      }
    }, error => {
      
        //this.pasInternet=true
        this.pasInterneet();
      
    });
  }

  async pasInterneet(){
    const alert = await this.alertController.create({
      header: 'Erreur',
      message: "Verifier votre internet",
      buttons: ['ok'],
    });
    await alert.present();
  }
 

}
