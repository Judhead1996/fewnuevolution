import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.page.html',
  styleUrls: ['./authentication.page.scss'],
})
export class AuthenticationPage implements OnInit {

  constructor(public  authService:AuthService, private route:Router, private alertController:AlertController) { }

  ngOnInit() {
  }

  async onRegister(value) {
    if(value.password == value.cpassword){
      this.authService.createUser({
        phone:value.phone,
        password:value.password,
        nom_complet:value.nom_complet,
      })
      .then(data =>{
        this.route.navigateByUrl('/login');
        console.log(data);
     }, err => {
        console.log(err);
     });
     console.log(value);
    }
      else{
        const alert = await this.alertController.create({
          header: 'Erreur',
          message: "Les Mots des passes ne sont pas les memes",
          buttons: ['ok'],
        });
        await alert.present();
      }
    }

}
